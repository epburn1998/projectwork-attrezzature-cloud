﻿using System;
using ContosoUniversity.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace UtenteDal
{

	public class Utente : DbContext
	{
		public Utente() : base("Utente")
		{
		}
		public DbSet<Utente> Utente { get; set; }
	}
}

