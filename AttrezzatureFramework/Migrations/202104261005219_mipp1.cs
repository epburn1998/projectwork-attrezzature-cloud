﻿namespace AttrezzatureFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mipp1 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Utente", new[] { "IDRuolo" });
            CreateTable(
                "dbo.Attrezzatura",
                c => new
                    {
                        codiceAttrezzatura = c.Int(nullable: false, identity: true),
                        statoDiFunzionamento = c.String(),
                        categoria = c.String(),
                        dataDiScadenza = c.DateTime(nullable: false),
                        assegnata = c.Boolean(nullable: false),
                        tipoManuale = c.String(),
                    })
                .PrimaryKey(t => t.codiceAttrezzatura);
            
            CreateTable(
                "dbo.AttrezzaturaInSquadra",
                c => new
                    {
                        IDAssegnazione = c.Int(nullable: false, identity: true),
                        codiceAttrezzatura = c.Int(nullable: false),
                        IDSquadra = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IDAssegnazione)
                .ForeignKey("dbo.Attrezzatura", t => t.codiceAttrezzatura)
                .ForeignKey("dbo.Squadra", t => t.IDSquadra)
                .Index(t => t.codiceAttrezzatura)
                .Index(t => t.IDSquadra);
            
            CreateTable(
                "dbo.UtenteInSquadra",
                c => new
                    {
                        IDSquadra = c.Int(nullable: false),
                        IDUtente = c.Int(nullable: false),
                        Ruolo = c.String(),
                    })
                .PrimaryKey(t => new { t.IDSquadra, t.IDUtente })
                .ForeignKey("dbo.Squadra", t => t.IDSquadra)
                .ForeignKey("dbo.Utente", t => t.IDUtente)
                .Index(t => t.IDSquadra)
                .Index(t => t.IDUtente);
            
            CreateTable(
                "dbo.Audit",
                c => new
                    {
                        IDAudit = c.Int(nullable: false, identity: true),
                        checkVisivoStrumentale = c.Boolean(nullable: false),
                        dataApertura = c.DateTime(nullable: false),
                        dataChiusura = c.DateTime(nullable: false),
                        esitoAudit = c.String(),
                        IDSquadra = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IDAudit)
                .ForeignKey("dbo.Squadra", t => t.IDSquadra)
                .Index(t => t.IDSquadra);
            
            CreateTable(
                "dbo.AuditGestore",
                c => new
                    {
                        IDAudit = c.Int(nullable: false),
                        IDUtente = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IDAudit, t.IDUtente })
                .ForeignKey("dbo.Audit", t => t.IDAudit)
                .ForeignKey("dbo.Utente", t => t.IDUtente)
                .Index(t => t.IDAudit)
                .Index(t => t.IDUtente);
            
            CreateTable(
                "dbo.AuditSquadra",
                c => new
                    {
                        IDAssegnazione = c.Int(nullable: false),
                        IDAudit = c.Int(nullable: false),
                        esitoAuditControllo = c.String(),
                    })
                .PrimaryKey(t => new { t.IDAssegnazione, t.IDAudit })
                .ForeignKey("dbo.AttrezzaturaInSquadra", t => t.IDAssegnazione)
                .ForeignKey("dbo.Audit", t => t.IDAudit)
                .Index(t => t.IDAssegnazione)
                .Index(t => t.IDAudit);
            
            AddColumn("dbo.Squadra", "IDUtente", c => c.Int(nullable: false));
            AddColumn("dbo.Squadra", "codiceAttrezzatura", c => c.Int(nullable: false));
            AlterColumn("dbo.Utente", "IDRuolo", c => c.Int(nullable: false));
            CreateIndex("dbo.Squadra", "codiceAttrezzatura");
            CreateIndex("dbo.Utente", "IDRuolo");
            AddForeignKey("dbo.Squadra", "codiceAttrezzatura", "dbo.Attrezzatura", "codiceAttrezzatura");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AuditSquadra", "IDAudit", "dbo.Audit");
            DropForeignKey("dbo.AuditSquadra", "IDAssegnazione", "dbo.AttrezzaturaInSquadra");
            DropForeignKey("dbo.AuditGestore", "IDUtente", "dbo.Utente");
            DropForeignKey("dbo.AuditGestore", "IDAudit", "dbo.Audit");
            DropForeignKey("dbo.Audit", "IDSquadra", "dbo.Squadra");
            DropForeignKey("dbo.AttrezzaturaInSquadra", "IDSquadra", "dbo.Squadra");
            DropForeignKey("dbo.UtenteInSquadra", "IDUtente", "dbo.Utente");
            DropForeignKey("dbo.UtenteInSquadra", "IDSquadra", "dbo.Squadra");
            DropForeignKey("dbo.Squadra", "codiceAttrezzatura", "dbo.Attrezzatura");
            DropForeignKey("dbo.AttrezzaturaInSquadra", "codiceAttrezzatura", "dbo.Attrezzatura");
            DropIndex("dbo.AuditSquadra", new[] { "IDAudit" });
            DropIndex("dbo.AuditSquadra", new[] { "IDAssegnazione" });
            DropIndex("dbo.AuditGestore", new[] { "IDUtente" });
            DropIndex("dbo.AuditGestore", new[] { "IDAudit" });
            DropIndex("dbo.Audit", new[] { "IDSquadra" });
            DropIndex("dbo.Utente", new[] { "IDRuolo" });
            DropIndex("dbo.UtenteInSquadra", new[] { "IDUtente" });
            DropIndex("dbo.UtenteInSquadra", new[] { "IDSquadra" });
            DropIndex("dbo.Squadra", new[] { "codiceAttrezzatura" });
            DropIndex("dbo.AttrezzaturaInSquadra", new[] { "IDSquadra" });
            DropIndex("dbo.AttrezzaturaInSquadra", new[] { "codiceAttrezzatura" });
            AlterColumn("dbo.Utente", "IDRuolo", c => c.Int());
            DropColumn("dbo.Squadra", "codiceAttrezzatura");
            DropColumn("dbo.Squadra", "IDUtente");
            DropTable("dbo.AuditSquadra");
            DropTable("dbo.AuditGestore");
            DropTable("dbo.Audit");
            DropTable("dbo.UtenteInSquadra");
            DropTable("dbo.AttrezzaturaInSquadra");
            DropTable("dbo.Attrezzatura");
            CreateIndex("dbo.Utente", "IDRuolo");
        }
    }
}
