﻿namespace AttrezzatureFramework.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AttrezzatureFramework.DAL.AttrezzatureDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AttrezzatureFramework.DAL.AttrezzatureDbContext context)
        {
            Ruolo admin = new Ruolo { descrizione = "Amministratore" };
            Ruolo utente = new Ruolo { descrizione = "Operaio" };

            if (context.Utenti.Count() == 0)
            {
                var utenti = new List<Utente>()
                            {
                                new Utente {Nome="Pierluigi",Cognome="Attanasio",Email="fordfocus2003@gmail.com",Pass="12345",Ruolo=admin },
                                new Utente {Nome="Angelo",Cognome="Depretis",Email="ferrari765@gmail.com",Pass="abcdef",Ruolo=utente },
                                new Utente {Nome="Luigi",Cognome="Mario",Email="devilman96@gmail.com",Pass="unoduetrequattro",Ruolo=utente },
                                new Utente {Nome="Astolfo",Cognome="Dallaluna",Email="dallalunastolfo@gmail.com",Pass="ghijklmno",Ruolo=utente },

                            };
                            utenti.ForEach(s => context.Utenti.Add(s));
                            context.SaveChanges();
            }

            if (context.Ruoli.Count() == 0)
            {
                

                context.Ruoli.Add(utente);
                context.Ruoli.Add(admin);

                context.SaveChanges();
            }
            
            

            /*DateTime date1 = new DateTime(2021, 27, 04, 00, 00, 00);
            DateTime date2 = new DateTime(2022, 27, 06, 00, 00, 00);
            DateTime date3 = new DateTime(2021, 12, 01, 00, 00, 00);
            DateTime date4 = new DateTime(2021, 29, 12, 00, 00, 00);
            var attrezzature = new List<Attrezzatura>()
            {
                new Attrezzatura {codiceAttrezzatura=1,statoDiFunzionamento="buono stato",categoria="strumentoA",dataDiScadenza=date1.Date,assegnata=true,tipoManuale="Manuale1" },
                new Attrezzatura {codiceAttrezzatura=2,statoDiFunzionamento="stato trasandato",categoria="strumentoB",dataDiScadenza=date2.Date,assegnata=false,tipoManuale="Manuale1"  },
                new Attrezzatura {codiceAttrezzatura=3,statoDiFunzionamento="buono stato",categoria="strumentoA",dataDiScadenza=date3.Date,assegnata=true,tipoManuale="Manuale2"  },
                new Attrezzatura {codiceAttrezzatura=4,statoDiFunzionamento="buono stato",categoria="strumentoC",dataDiScadenza=date4.Date, assegnata=false,tipoManuale="Manuale4"  },

            };
            attrezzature.ForEach(s => context.Attrezzature.Add(s));
            context.SaveChanges();*/

            /*var fornitore = new List<Fornitore>()
            {
                new Fornitore {IDFornitore=1, partitaIVA="acciu", ragioneSociale="ragiono" },
                new Fornitore {IDFornitore=2, partitaIVA="sette", ragioneSociale="io no"  },
                new Fornitore {IDFornitore=3, partitaIVA="si", ragioneSociale="ok"   },
                new Fornitore {IDFornitore=4, partitaIVA="evvai", ragioneSociale="ragiono"   },

            };
            fornitore.ForEach(s => context.Fornitore.Add(s));
            context.SaveChanges(); */

            /*var ruolo = new List<Ruolo>()
            {
                new Ruolo {IDRuolo=1, descrizione="ottima qualità"},
                new Ruolo {IDRuolo=2, descrizione="molto bello"},
                new Ruolo {IDRuolo=3, descrizione="utile"},
                new Ruolo {IDRuolo=4, descrizione="evvai"},

            };
            ruolo.ForEach(s => context.Ruolo.Add(s));
            context.SaveChanges(); */

            /*var squadra = new List<Squadra>()
             {
                 new Squadra {IDSquadra=1, codiceAttrezzatura=1,componentiSquadra=2},
                 new Squadra {IDSquadra=2, codiceAttrezzatura=3,componentiSquadra=2},
                 new Squadra {IDSquadra=3, codiceAttrezzatura=4,componentiSquadra=2},
                 new Squadra {IDSquadra=4, codiceAttrezzatura=2,componentiSquadra=2},

             };
             squadra.ForEach(s => context.Squadra.Add(s));
             context.SaveChanges();*/

            /*DateTime date1 = new DateTime(2021, 27, 04, 00, 00, 00);
            DateTime date2 = new DateTime(2022, 27, 06, 00, 00, 00);
            DateTime date3 = new DateTime(2021, 12, 01, 00, 00, 00);
            DateTime date4 = new DateTime(2021, 29, 12, 00, 00, 00);
            var audit = new List<Audit>()
            {
                new Audit {IDAudit=1, checkVisivoStrumentale=true,IDSquadra=1,dataApertura=date1.Date,dataChiusura=date4.Date,esitoAudit="positivo" },
                new Audit {IDAudit=2, checkVisivoStrumentale=true,IDSquadra=4,dataApertura=date3.Date,dataChiusura=date2.Date,esitoAudit="positivo"},
                new Audit {IDAudit=3, checkVisivoStrumentale=false,IDSquadra=3,dataApertura=date2.Date,dataChiusura=date3.Date,esitoAudit="negativo"},
                new Audit {IDAudit=4, checkVisivoStrumentale=false,IDSquadra=2,dataApertura=date4.Date,dataChiusura=date1.Date,esitoAudit="positivo"},

            };
            audit.ForEach(s => context.Audit.Add(s));
            context.SaveChanges();*/

            //questa parte di codice serve per popolare le tabelle una volta fatta la migrazione
            /*var auditsquadra = new List<AuditSquadra>()
            {
                new AuditSquadra {IDAssegnazione=12, IdAssegnazione=1,IDaudit=2,esitoAuditControllo="si"},
                new AuditSquadra {IDAssegnazione=23, IdAssegnazione=2,IDaudit=3,esitoAuditControllo="no"},
                new AuditSquadra {IDAssegnazione=32, IdAssegnazione=3,IDaudit=2,esitoAuditControllo="si"},
                new AuditSquadra {IDAssegnazione=44, IdAssegnazione=4,IDaudit=4,esitoAuditControllo="no"},

            };
            auditsquadra.ForEach(s => context.AuditSquadra.Add(s));
            context.SaveChanges(); */

            /*var attrezzaturesquadra = new List<AttrezzaturaInSquadra>()
            {
                new AttrezzaturaInSquadra {IDAssegnazione=1, codiceAttrezzatura=3,IDSquadra=1},
                new AttrezzaturaInSquadra {IDAssegnazione=2, codiceAttrezzatura=1,IDSquadra=2},
                new AttrezzaturaInSquadra {IDAssegnazione=3, codiceAttrezzatura=2,IDSquadra=4},
                new AttrezzaturaInSquadra {IDAssegnazione=4, codiceAttrezzatura=4,IDSquadra=3},

            };
            attrezzaturesquadra.ForEach(s => context.AttrezzaturaInSquadra.Add(s));
            context.SaveChanges();*/

            /*var utenteinsquadra = new List<UtenteInSquadra>()
            {
                new UtenteInSquadra {IDUtente=1,IDSquadra=2,ruolo="Caposquadra"},
                new UtenteInSquadra {IDUtente=2,IDSquadra=4,ruolo="operaio semplice"},
                new UtenteInSquadra {IDUtente=3,IDSquadra=3,ruolo="Caposquadra"},
                new UtenteInSquadra {IDUtente=4,IDSquadra=1,ruolo="Caposquadra"},

            };
            utenteinsquadra.ForEach(s => context.UtenteInSquadra.Add(s));
            context.SaveChanges();*/

            /*var auditgestore = new List<AuditGestore>()
            {
                new AuditGestore {IDGestore=12,IDUtente=1,IDAudit=2,},
                new AuditGestore {IDGestore=23,IDUtente=2,IDAudit=3},
                new AuditGestore {IDGestore=33,IDUtente=3,IDAudit=3},
                new AuditGestore {IDGestore=43,IDUtente=4,IDAudit=3},

            };
            auditgestore.ForEach(s => context.AuditGestore.Add(s));
            context.SaveChanges();*/


            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }
}
