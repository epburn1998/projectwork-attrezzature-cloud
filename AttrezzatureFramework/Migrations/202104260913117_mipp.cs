﻿namespace AttrezzatureFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mipp : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Squadra",
                c => new
                    {
                        IDSquadra = c.Int(nullable: false, identity: true),
                        componentiSquadra = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IDSquadra);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Squadra");
        }
    }
}
