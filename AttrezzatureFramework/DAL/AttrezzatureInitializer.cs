﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace AttrezzatureFramework.DAL
{
    public class AttrezzatureInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<AttrezzatureDbContext>
    {
        protected override void Seed(AttrezzatureDbContext context)
        {
            var utenti = new List<Utente>()
            {
                new Utente {Nome="Pierluigi",Cognome="Attanasio",Email="fordfocus2003@gmail.com",Pass="12345" },
                new Utente {Nome="Angelo",Cognome="Depretis",Email="ferrari765@gmail.com",Pass="abcdef" },
                new Utente {Nome="Luigi",Cognome="Mario",Email="devilman96@gmail.com",Pass="unoduetrequattro" },
                new Utente {Nome="Astolfo",Cognome="Dallaluna",Email="dallalunastolfo@gmail.com",Pass="ghijklmno" },

            };
            utenti.ForEach(s => context.Utenti.Add(s));
            context.SaveChanges();
        }
    }
}
