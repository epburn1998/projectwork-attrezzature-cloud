﻿using System;
using AttrezzatureFramework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


namespace AttrezzatureFramework.DAL
{
    public class AttrezzatureDbContext : DbContext
    {
        public AttrezzatureDbContext() : base("AttrezzatureDbContext")
        {
        }

       

        public DbSet<Utente> Utenti { get; set; }
        public DbSet<Squadra> Squadre { get; set; }
        public DbSet<Attrezzatura> Attrezzature { get; set; }
        public DbSet<Audit> Audits { get; set; }
        public DbSet<UtenteInSquadra> UtentiInSquadra {get;set;}
        public DbSet<AttrezzaturaInSquadra> attrezzatureInSquadra { get; set; }
        public DbSet<AuditGestore> AuditsGestore { get; set; }
        public DbSet<AuditSquadra> AuditsSquadra { get; set; }

        public DbSet<Ruolo> Ruoli { get; set; }




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToOneConstraintIntroductionConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
