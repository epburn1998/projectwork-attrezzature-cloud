﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows;
using System.Web;
using System.Security.Cryptography;//using per la cifratura


namespace AttrezzatureFramework.DAL
{
    public class Cifrario
    {
        //come faccio a non inserire la chiave di cifratura in chiaro nel codice?
        private string chiave = "aswqdfre1324fgtr5764jkiu8769lo90";//32 caratteri chiave di crittografia
        private string vettore = "qqqwwe34hj789kii";//16 caratteri
        public string Cript(string p)
        {
            //prende in input una stringa in chiaro

            byte[] PasswordDaCriptare = ASCIIEncoding.ASCII.GetBytes(p);
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;//bit
            aes.KeySize = 256;//caratteri chiave provata
            aes.Key = ASCIIEncoding.ASCII.GetBytes(chiave);
            aes.IV = ASCIIEncoding.ASCII.GetBytes(vettore);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            ICryptoTransform ict = aes.CreateEncryptor(aes.Key, aes.IV);
            byte[] testocriptato = ict.TransformFinalBlock(PasswordDaCriptare, 0, PasswordDaCriptare.Length);
            ict.Dispose();

            string s = Convert.ToBase64String(testocriptato);

            return s;
            //restituisce la stringa criptata

        }
    }
}
