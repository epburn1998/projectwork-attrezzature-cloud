﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework
{
    public class AttrezzaturaInSquadra
    {
        [Key]
        public int IDAssegnazione { get; set; }
        
        [ForeignKey("Attrezzatura")]
        public int codiceAttrezzatura { get; set; }
        public Attrezzatura Attrezzatura { get; set; }

        [ForeignKey("Squadra")]
        public int IDSquadra { get; set; }
        public Squadra Squadra { get; set; }
    }
}
