﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework
{
    public class Attrezzatura
    {
        [Key]

        //[MaxValue(100)]
        public int codiceAttrezzatura { get; set; }
        //[MaxLength(20)]
        public string statoDiFunzionamento { get; set; }
        //[MaxLength(20)]
        public string categoria { get; set; }
        //[MaxLength(8)][MaxValue(8)]
        public DateTime dataDiScadenza { get; set; }
        //[MaxLength(2)][MaxValue(2)]
        public bool assegnata { get; set; }
        //[MaxLength(20)]
        public string tipoManuale { get; set; }

    }
}
