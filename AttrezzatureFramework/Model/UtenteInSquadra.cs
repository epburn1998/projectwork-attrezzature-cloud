﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework
{
    public class UtenteInSquadra
    {
        //merge di due dati  
        //IDUtente e IDSquadra


        //[MaxLength(20)]
        public string Ruolo { get; set; }

        [Key, Column(Order = 0)]
        [ForeignKey("Squadra")]
        //[MaxValue(10)]
        public int IDSquadra { get; set; }
        public Squadra Squadra{ get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Utente")]
        //[MaxValue(100)]
        public int IDUtente { get; set; }
        public Utente Utente { get; set; }


    }
}
