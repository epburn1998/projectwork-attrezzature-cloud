﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework
{
    public class AuditSquadra
    {
       
       [Key, Column(Order = 0)]
       [ForeignKey("AttrezzaturaInSquadra")]
       public int IDAssegnazione { get; set; }
       public AttrezzaturaInSquadra AttrezzaturaInSquadra { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Audit")]
        public int IDAudit { get; set; }
        public virtual Audit Audit { get; set; }
       // [MaxLength(15)]
        public string esitoAuditControllo { get; set; }
    }
}
