﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework
{
    public class Audit
    {
        [Key]
        //[MaxValue(100)]
        public int IDAudit { get; set; }
        // [MaxLength(2)][MaxValue(2)]
        public bool checkVisivoStrumentale { get; set; }
        //[MaxLength(8)][MaxValue(8)]
        public DateTime dataApertura { get; set; }
        // [MaxLength(8)][MaxValue(8)]
        public DateTime dataChiusura { get; set; }
       // [MaxLength(15)]
        public string esitoAudit { get; set; }

        [ForeignKey("Squadra")]
        public int IDSquadra { get; set; }
        public Squadra Squadra { get; set; }
    }
}
