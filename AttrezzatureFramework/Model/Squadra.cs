﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework
{
    
    public class Squadra
    {
        [Key]
        //[MaxValue(100)]
        public int IDSquadra { get; set; }

        //[MaxValue(2)]
        public int componentiSquadra { get; set; }

        public virtual ICollection<UtenteInSquadra> utentiInSquadra { get; set; }

        //[MaxValue(100)]
        public int IDUtente { get; set; }
        

        [ForeignKey("Attrezzatura")]
        public int codiceAttrezzatura { get; set; }
        public Attrezzatura Attrezzatura { get; set; }
    }
}
