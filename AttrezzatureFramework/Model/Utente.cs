﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AttrezzatureFramework
{
    public class Utente
    {
        [Key]
        //[MaxValue(50)]
        public int ID { get; set; }
       // [MaxLength(10)]
       [Required(ErrorMessage ="Campo obbligatorio")]
        public string Nome { get; set; }
        // [MaxLength(10)]
        [Required(ErrorMessage = "Campo obbligatorio")]
        public string Cognome { get; set; }
        // [MaxLength(50)]
        [Required(ErrorMessage = "Campo obbligatorio")]
        public string Email { get; set; }

        //non settiamo un valore massimo per la password per motivi di security
        [Required(ErrorMessage = "Campo obbligatorio")]
        public string Pass { get; set; }

        [ForeignKey("Ruolo")]
        //[MaxValue(10)]
        public int IDRuolo { get; set; }
        public Ruolo Ruolo { get; set; }

        [ForeignKey("Fornitore")]
        //[MaxValue(10)]
        public int? IDFornitore { get; set; }
        public Fornitore Fornitore { get; set; }
    }
}