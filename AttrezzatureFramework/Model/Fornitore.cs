﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace AttrezzatureFramework
{
    public class Fornitore
    {
        [Key]
        //[MaxValue(100)]
        public int IDFornitore { get; set; }
        //[MaxLength(50)]
        public string partitaIVA { get; set; }
       // [MaxLength(50)]
        public string ragioneSociale { get; set; }
    }
}
