﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AttrezzatureFramework.DAL;

namespace AttrezzatureFramework.CreaAccount
{
    public class CreateAccount
    {

        AttrezzatureDbContext dbContext = new AttrezzatureDbContext();

        public void newAccountCreate(string email,string nome, string cognome, string password)
        {
            try
            {
                Utente user = new Utente { Nome = nome, Cognome = cognome, Email = email, Pass = password};
                dbContext.Utenti.Add(user);
                dbContext.SaveChanges();
            }
            catch (Exception crEx)
            {
                Console.WriteLine(crEx.Message);
            }
        }
    }
}
