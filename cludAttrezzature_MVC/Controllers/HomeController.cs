﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;


namespace cludAttrezzature_MVC.Controllers
{
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login()
        {
            
            ViewBag.Message = "Login";
            return View();
        }
    

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }
    }
}