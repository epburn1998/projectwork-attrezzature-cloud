﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cludAttrezzature
{
    public partial class Contact : Page
    {
        DataTable dtEventi = new DataTable();
        

        protected void Page_Load(object sender, EventArgs e)
        {
            dtEventi.Columns.Add("Data", typeof(DateTime));
            dtEventi.Columns.Add("Evento", typeof(string));
            dtEventi.Rows.Add(new DateTime(2020, 1, 1),"Rifornimento Attrezzature");
            dtEventi.Rows.Add(new DateTime(2020, 1, 2), "");
            dtEventi.Rows.Add(new DateTime(2020, 1, 3), "");
            dtEventi.Rows.Add(new DateTime(2020, 1, 4), "");
            dtEventi.Rows.Add(new DateTime(2020, 1, 5), "Esaurimento Scorte");


           
            
        }

        protected void Data1_TextChanged(object sender, EventArgs e)
        {
            
        }

        protected void Data2_TextChanged(object sender, EventArgs e)
        {
           
        }

        protected void DateSubmit_Click(object sender, EventArgs e)
        {


            //DataRow[] eventi = dtEventi.Select(string.Format("Data>=#{0}# AND Data<=#{1}#",TXData1.Text,TXData2.Text)); //Da togliere


            //DateEventiGridView.DataSource = dtEventi;
            //DateEventiGridView.DataBind();

            //DataView per filtrare le date
            DataView dtEventiDataView = new DataView(dtEventi);
            dtEventiDataView.RowFilter = string.Format("Data>=#{0}# AND Data<=#{1}#", TXData1.Text, TXData2.Text);

            DateEventiGridView.DataSource = dtEventiDataView;
            DateEventiGridView.DataBind();//DataBind per impostare la GridView
            //foreach (DataRow row in eventi)
            //{
            //    string evento = row["Evento"].ToString();
            //    DateLabels.Text += row["Data"] +" " +evento + "<br>";   //Da togliere
            //}



        }

        
    }
}