﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;//import per il controllo caratteri textbox
using AttrezzatureFramework.DAL;//per dbContext

namespace cludAttrezzature
{
    public partial class ResetPasswordForm : System.Web.UI.Page
    {
        //variabile otp per il passaggio alla pagina ResetPasswordConfirmForm 
        public static int otp = 0;

        public AttrezzatureDbContext dbContext = new AttrezzatureDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click1(object sender, EventArgs e)
        {   
            //generiamo otp random:
            Random rnd = new Random();
            otp = rnd.Next(1000, 9999);

            //se user è corretto visualizziamo  otp:
            //if (TextBox1.Text == "admin") // Da aggiungere, se il nome esiste nel database SQL
            if (dbContext.Utenti.Where(r => r.Email == TextBox1.Text && r.Pass == TextBox2.Text).Count() > 0)
            {
                
                string message = "il codice otp è:";
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                sb.Append("<script type = 'text/javascript'>");

                sb.Append("window.onload=function(){");

                sb.Append("alert('");

                sb.Append(message);

                sb.Append(otp);

                sb.Append("')};");

                sb.Append("</script>");

                ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
                try
                {
  
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");


                    mail.From = new MailAddress("your_email_address@gmail.com");
                    mail.To.Add("to_address");
                    mail.Subject = "Software Attrezzature Cloud: Password OTP";
                    mail.Body = "Ciao admin, la tua password otp é " + otp + ". \n\n\n Per reimpostare la password clicca il link seguente:\n [LINK]";

                    SmtpServer.Port = 587;
                    SmtpServer.Credentials = new System.Net.NetworkCredential("admin", "admin");
                    SmtpServer.EnableSsl = true;

                    SmtpServer.Send(mail);
                    //MessageBox.Show("mail Send");
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.ToString());
                }


            }

        }

        protected void BTreimpostapw_Click(object sender, EventArgs e)
        {
            //per il passaggio della otp alla pagina ResetPasswordConfirmForm utilizzo un Cookie:

            HttpCookie cookie = new HttpCookie("otp");
            cookie.Value = otp.ToString();
            Response.Cookies.Add(cookie);
            Response.Redirect("ResetPasswordConfirmForm.aspx");

            

        }
    }
}