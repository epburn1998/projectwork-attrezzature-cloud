﻿<%@ Page Language="C#" Title="Registrazione" AutoEventWireup="true"  MasterPageFile="~/Site.Master" CodeBehind="RegisterForm.aspx.cs" Inherits="cludAttrezzature.RegisterForm" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1><asp:Label runat="server" Text="Registrazione Nuovo Utente"></asp:Label></h1>
    <br />
    <br />

    <asp:Label runat="server" Text="Inserisci indirizzo email"></asp:Label><br />
    <asp:TextBox runat="server" ID="emailTXBox" Width="167px"></asp:TextBox> <asp:RegularExpressionValidator ID="regexEmailValid" ForeColor="Red" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="emailTXBox" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator><br />
    <asp:RequiredFieldValidator runat="server" id="reqEmail" ForeColor="Red" controltovalidate="emailTXBox" errormessage="Campo obbligatorio" /><br />

    <asp:Label runat="server" Text="Nome"></asp:Label><br />
    <asp:TextBox runat="server" ID="nomeTXBox" Width="167px" ></asp:TextBox>
    <asp:RequiredFieldValidator runat="server" id="reqNome" ForeColor="Red" controltovalidate="nomeTXBox" errormessage="Campo obbligatorio" /><br />
    
    <asp:Label runat="server" Text="Cognome"></asp:Label><br />
    <asp:TextBox runat="server" ID="cognomeTXBox" Width="167px"></asp:TextBox>
    <asp:RequiredFieldValidator runat="server" id="reqCognome" ForeColor="Red" controltovalidate="cognomeTXBox" errormessage="Campo obbligatorio" /><br />

    <asp:Label runat="server" Text="Inserisci Password"></asp:Label><br />
    <asp:TextBox runat="server" ID="passTXBox" Width="165px"></asp:TextBox>
    <asp:RequiredFieldValidator runat="server" id="reqPass" ForeColor="Red" controltovalidate="passTXBox" errormessage="Password obbligatoria" /><br />

    <asp:Label runat="server" Text="Conferma Password"></asp:Label><br />
    <asp:TextBox runat="server" ID="confermaPassTXBox" Width="160px"></asp:TextBox><br />
    <asp:CompareValidator runat="server" ID="checkPass" ForeColor="Red" Operator="Equal" controlToValidate="confermaPassTXBox" controlToCompare="passTxBox" ErrorMessage="Le password non corrispondono"></asp:CompareValidator><br />


    <asp:LinkButton ID="CogLinkButton" CssClass="btn btn-default btn-lg" runat="server" Text="Registrati" Width="138px" OnClick="CogLinkButton_Click"></asp:LinkButton>



</asp:Content>