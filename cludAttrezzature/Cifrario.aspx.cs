﻿using System;
using System.Security.Cryptography;//using per la cifratura
using System.Text;//using per la cifratura

namespace cludAttrezzature
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        private string chiave = "aswqdfre1324fgtr5764jkiu8769lo90";//32 caratteri chiave di crittografia
        private string vettore = "qqqwwe34hj789kii";//16 caratteri
        protected void Page_Load(object sender, EventArgs e)
        {


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            byte[] PasswordDaCriptare = ASCIIEncoding.ASCII.GetBytes(PasswordDacriptareTXT.Text);
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;//bit
            aes.KeySize = 256;//caratteri chiave provata
            aes.Key = ASCIIEncoding.ASCII.GetBytes(chiave);
            aes.IV = ASCIIEncoding.ASCII.GetBytes(vettore);
            //aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            ICryptoTransform ict = aes.CreateEncryptor(aes.Key, aes.IV);
            byte[] testocriptato = ict.TransformFinalBlock(PasswordDaCriptare, 0, PasswordDaCriptare.Length);
            ict.Dispose();

            PasswordCriptataTXT.Text = Convert.ToBase64String(testocriptato);




        }

    }
}