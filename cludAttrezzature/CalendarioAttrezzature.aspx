﻿<%@ Page Title="Calendario Eventi" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CalendarioAttrezzature.aspx.cs" Inherits="cludAttrezzature.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Calendario
    </h3>

    
    
    <asp:TextBox ID="TXData1" runat="server" TextMode="Date" OnTextChanged="Data1_TextChanged"></asp:TextBox>
    <asp:TextBox ID="TXData2" runat="server" TextMode="Date" OnTextChanged="Data2_TextChanged"></asp:TextBox><br /><br />
    <asp:Button ID="DateSubmit" runat="server" OnClick="DateSubmit_Click" Text="Invia"/><br /><br />
    <asp:Label ID="DateLabels" runat="server"></asp:Label>
    <asp:GridView runat="server" ID="DateEventiGridView" AutoGenerateColumns="False" Width="300px">
        <Columns>
            <asp:BoundField DataField="Data" HeaderText="Data"/>
            <asp:BoundField DataField="Evento" HeaderText="Evento" />
        </Columns>

    </asp:GridView>
</asp:Content>
