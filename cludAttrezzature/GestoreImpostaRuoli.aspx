﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GestoreImpostaRuoli.aspx.cs" Inherits="cludAttrezzature.GestoreImpostaRuoli" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   <h1><asp:Label runat="server" Text="Gestione Ruoli"></asp:Label></h1>
    <br />
    <asp:GridView runat="server" AutoGenerateColumns="false" ID="gridViewListaUtenti" CssClass="table table-striped">
        <Columns>
            <asp:BoundField DataField="Nome" HeaderText="Nome"/>
            <asp:BoundField DataField="Cognome" HeaderText="Cognome"/>
            <asp:BoundField DataField="Email" HeaderText="Email"/>
            <asp:TemplateField HeaderText = "Ruolo">
                <ItemTemplate>
                    <asp:Label ID="ruoloField" runat="server" Text='Ruolo' Visible = "false" />
                    <asp:DropDownList runat="server" ID="ddlRuoli"></asp:DropDownList>
                </ItemTemplate>
        </asp:TemplateField>
        </Columns>
    </asp:GridView>
   
    
   
</asp:Content>
