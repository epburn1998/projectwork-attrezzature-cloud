﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AttrezzatureFramework;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;//import per il controllo caratteri textbox
using AttrezzatureFramework.DAL;




namespace cludAttrezzature
{
    public partial class About : Page
    {

        string capt = "";
        string queryStringIDD = "";
        static string cooki;

        int i = 1;

        public AttrezzatureDbContext dbContext = new AttrezzatureDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {


            //codice per generare il captcha:
            Random r = new Random();
            int n = r.Next(6, 10);
            int tot = 0;

            do
            {
                /*
                 * 0-9 = 48-57
                 * A-Z = 65-90
                 * a-z = 97-122
                 */
                int c = r.Next(48, 123);
                if ((c >= 48 && c <= 57) || (c >= 65 && c <= 90) || (c >= 97 && c <= 122))
                {
                    capt = capt + (char)c;
                    tot++;
                    if (tot == n)
                        break;
                }
            }
            while (true);

            //la Label4 assume il valore generato e viene visualizzato
            Label4.Text = capt;
            queryStringIDD = Label4.Text;

            //il valore viene memorizzato in un cookie
            HttpCookie cookie = new HttpCookie("queryStringIDD");
            cookie.Value = queryStringIDD.ToString();
            Response.Cookies.Add(cookie);
            cooki = (Request.Cookies["queryStringIDD"].Value);
        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {

        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {





            //ok tentiamo la connessione al db con l'Entity Framework
            if (dbContext.Utenti.Where(r => r.Email == TextBox1.Text && r.Pass == TextBox2.Text).Count() > 0)
            {

                i = 0;//variabile per lo switch case
                      //fa questo ->  PasswordNotValidCaseLabel.Text = "";
                      //Response.Redirect("CalendarioAttrezzature.aspx");
            }
            else
            {

                i = 1;//variabile per lo switch case
                      //fa questo -> ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Password o email errata!');", true);
            }
            if (TextBox3.Text == "")
            {

                i = 2;//variabile per lo switch case
                      //Captcha non settato! 
                      //fa questo -> ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Campo Captcha non settato!');", true);
            }
            if ((TextBox3.Text != "")&&(!(TextBox3.Text.Equals(cooki))))
            {
                i = 3;//variabile per lo switch case
                      //Captcha errato! 
                      //fa questo -> ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Captcha errato!');", true);
            }



            switch (i)
            {
                case 0:
                    PasswordNotValidCaseLabel.Text = "";
                    Response.Redirect("CalendarioAttrezzature.aspx");
                    break;
                case 1:
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Password o email errata! Hai dimenticato la password?');", true);
                    break;
                case 2:
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Campo Captcha non settato!');", true);
                    break;
                case 3:
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Captcha errato!');", true);
                    break;

            }
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Unnamed2_Click(object sender, EventArgs e)
        {

        }

    }
}