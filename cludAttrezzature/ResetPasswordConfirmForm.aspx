﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPasswordConfirmForm.aspx.cs" Inherits="cludAttrezzature.ResetPasswordConfirmForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label3" runat="server" Text="Inserisci codice OTP"></asp:Label><br />



            <asp:TextBox runat="server" TextMode="Password" ID="OTP"></asp:TextBox><br />
            

            <br />
            <asp:Label ID="NewPasswordLabelText" runat="server" Text="Inserisci Nuova Password"
               TargetControlID="Password" RequiresUpperAndLowerCaseCharacters="true"
                MinimumNumericCharacters="10" MinimumSymbolCharacters="10" 
                PreferredPasswordLength="12" DisplayPosition="RightSide" 
                StrengthIndicatorType="Text"></asp:Label> <br />


            <asp:TextBox runat="server" TextMode="Password" ID="PassTextBox"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="ConfirmNewPasswordLabelText" runat="server" Text="Conferma Nuova Password"
                TargetControlID="Password" RequiresUpperAndLowerCaseCharacters="true"
                MinimumNumericCharacters="10" MinimumSymbolCharacters="10" 
                PreferredPasswordLength="12" DisplayPosition="RightSide" 
                StrengthIndicatorType="Text"></asp:Label><br />



            <asp:TextBox runat="server" TextMode="Password" ID="ConfirmTextBox"></asp:TextBox>
      </div>
        
        <p>
            &nbsp;</p>
        <p>
     <p>
        La password deve contenere almeno 10 caratteri delle seguenti categorie:<br />
        Lettere maiuscole delle lingue dell'Europa (da A a Z)<br />
        Lettere minuscole delle lingue dell'Europa (a-z)<br />
        Cifre in base 10 (da 0 a 9)<br />
        Caratteri speciali: (~!@#$%^&*-+=) I simboli di valuta non sono caratteri speciali <br /><br />
     </p> 


            <asp:Button ID="Button1" runat="server" Text="Reimposta" OnClick="Button1_Click" />
        </p>
       
    </form>

           
</body>
</html>
