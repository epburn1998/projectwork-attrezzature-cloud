﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AttrezzatureFramework;
using MimeKit;
using MailKit.Net.Smtp;

namespace cludAttrezzature
{
    public partial class RecuperoPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void sendEmail_Click(object sender, EventArgs e)
        {
            using (AttrezzatureFramework.DAL.AttrezzatureDbContext dbContext = new AttrezzatureFramework.DAL.AttrezzatureDbContext())
            {
                if (dbContext.Utenti.Where(r => r.Email == emailTxtBox.Text).Count() > 0)
                {
                    //Send email
                    MimeMessage messageM = new MimeMessage();
                    messageM.From.Add(new MailboxAddress("from", "email@prova.it"));  //Indirizzo mittente
                    messageM.To.Add(new MailboxAddress("to", "email@prova.it"));        //Destinatario (email da recuperare)
                    messageM.Subject = "ciao";
                    messageM.Body = new TextPart("plain")
                    {
                        Text = "prova prova prova"
                    };

                    using (var client = new SmtpClient())
                    {
                        //Demo
                        client.ServerCertificateValidationCallback = (s, c, h, ev) => true;

                        client.Connect("smtp.live.com", 587); //SMTP su dove ci andiamo a connettere

                        client.Authenticate("email", "password");  //Da sosittuire con l'email con cui ci connettiamo all'SMTP ( e password)

                        try
                        {

                            client.Send(messageM);
                            client.Disconnect(true);
                            labelResult.ForeColor = System.Drawing.Color.Black;
                            labelResult.Text = "Abbiamo inviato un email a " + emailTxtBox.Text+".";

                        }
                        catch (Exception exc)
                        {
                            labelResult.Text = exc.Message;
                        }

                    }







                }
                else
                {
                    labelResult.Text = "Non risulta nessuna email " + emailTxtBox.Text + " nel database";
                }
            }
        }
    }
}
    
