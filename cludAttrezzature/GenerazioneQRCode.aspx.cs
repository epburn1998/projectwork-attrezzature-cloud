﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using QRCoder;

namespace cludAttrezzature
{
    public partial class Generazione_QRCode : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGeneraQR_Click(object sender, EventArgs e)
        {
            string codice = txtBoxCodice.Text;
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(codice, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            
            System.Web.UI.WebControls.Image imageQRCode = new System.Web.UI.WebControls.Image();
            imageQRCode.Height = 150;
            imageQRCode.Width = 150;

            using (MemoryStream ms= new MemoryStream())
            {     
                using (Bitmap bitmap = qrCode.GetGraphic(20))
                {
                    bitmap.Save(ms, ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();
                    imageQRCode.ImageUrl = "data::image/png;base64," + Convert.ToBase64String(byteImage);
                }
                placeholderQR.Controls.Add(imageQRCode);
            }
           

            
            
        }
    }
}