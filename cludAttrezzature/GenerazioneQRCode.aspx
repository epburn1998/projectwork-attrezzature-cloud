﻿<%@ Page Title="Generazione QR Code" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GenerazioneQRCode.aspx.cs" Inherits="cludAttrezzature.Generazione_QRCode" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label ID="firstLabel" runat="server" Text="Inserisci codice"></asp:Label><br />
    <asp:TextBox ID="txtBoxCodice" runat="server"></asp:TextBox><br />
    <asp:Button ID="btnGeneraQR" runat="server" Text="Genera QR" OnClick="btnGeneraQR_Click" /><br /><br />
    <asp:Placeholder runat="server" ID="placeholderQR"></asp:Placeholder>
    
</asp:Content>