﻿using System;
using System.Collections.Generic;

namespace cludAttrezzature
{
    public partial class ResetPasswordConfirmForm : System.Web.UI.Page
    {
        
        string queryStringID = "";
        static string risultato = "";
        static string ris = "";

        //funzione per controllo presenza caratteri speciali
        public bool MioConfronto(string stringa)
        {
            string s5 = stringa;
            int n = 0;
            for (int y = 0; y < s5.Length; y++)
            {
                if (s5[y]=='@'|| s5[y] == '!' || s5[y] == '~')
                {
                    n++;
                }
                else if (s5[y] == '#' || s5[y] == '$' || s5[y] == '%')
                {
                    n++;
                }
                else if (s5[y] == '&' || s5[y] == '*' || s5[y] == '^')
                {
                    n++;
                }
                else if (s5[y] == '-' || s5[y] == '+' || s5[y] == '=')
                {
                    n++;
                }
            }
            if (n>0)
              {
                return false;
              }
              else
              {
                return true;
              }
        }
        //fine funzione per controllo presenza caratteri speciali

        //funzione per controllo presenza lettere maiuscole nella password
        public object Parse_string_m(string str)
          {
            int n;
            int upper = 0;           
            int val = str.Length - 1;
            char[] c;
            for (n = 0; n <= (val); n++)
            {
                c = (str.ToCharArray(n, 1));
                char a = c[0];
                if ((Char.IsUpper(a)))
                {
                    upper=upper+1;
                }
            }
                if ((upper != 0))
                {
                    ris = "La stringa contiene Lettere maiuscole";
                }
                else 
                {    
                    ris = "La stringa non contiene Lettere maiuscole";  
                }
            return ris;
        }//aggiungere  ----> IsWhiteSpace(Char) nella funzione per controllo presenza spazi bianchi
         //fine funzine per verifica lettere maiuscole

         
        //funzione Parse_string per controllo caratteri nella password:
        public object Parse_string(string stringa)
        {
            int n;
            int lettere = 0;           
            int numeri = 0;
            int val = stringa.Length - 1;
            char[] c;

            for (n = 0; n <= (val); n++)
            {
                c = (stringa.ToCharArray(n, 1));
                char a = c[0];
                if ((Char.IsLetter(a)))
                {
                    lettere = lettere + 1;
                }
                else
                {
                    if ((char.IsNumber(a)))
                    {
                        numeri = numeri + 1;
                    }
                }
            }
            if ((lettere != 0) & (numeri != 0))
            {
                risultato = "La stringa contiene Lettere e Numeri";
            }
            else
            {
                if ((lettere != 0))
                {
                    risultato = "La stringa contiene solo Lettere";
                }
                else 
                {
                    if ((numeri != 0))
                    {
                        risultato = "La stringa contiene solo Numeri";
                    }
                }
            }
            return risultato;
        }
        //fine della funzione Parse_string per controllo caratteri nella password

        

        protected void Page_Load(object sender, EventArgs e)
        {

            //controllo otp corretto 
            
            if (Request.Cookies["otp"] != null)
            {
                queryStringID = (Request.Cookies["otp"].Value);
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
          

            int i = 0;
            // Regex rgx = new Regex("~!@#$%^&*-+=");//variabile per il controllo presenza caratteri speciali nella password
            //string rgx = "~!@#$%^&*-+=";
            //char[] charSpecial = new char[] { '~', '!', '@', '#', '$', '%', '&', '*', '^' };

            //controllo otp corretto: 
            if (OTP.Text.Equals(queryStringID))
            {

             //controllo generale se totto è ok:
             if ((PassTextBox.Text.Equals(ConfirmTextBox.Text)))
             {

                 i = 1; //variabile per lo switch case
                       //SUCCESSO!
                       //fa questo -> Response.Redirect("About.aspx");

             }
            
             if ((PassTextBox.Text != "") && (PassTextBox.Text != ConfirmTextBox.Text))
             {

                i = 2;//variabile per lo switch case
                      //campi password differenti
                      //fa questo -> ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Le password non coincidono!');", true);
             }

             if (PassTextBox.Text == "")
             {

                i = 3;//variabile per lo switch case
                      //campo password vuoto
                      //fa questo -> ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Campo password non settato!');", true);
             }

             if ((PassTextBox.Text != "") && (PassTextBox.Text.Length<10) )
             {
                i = 4;//variabile per lo switch case
                      //lunghezza minima password
                      //fa questo -> ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('La Password deve contenere almeno 10 caratteri!');", true);

             }

                //controllo presenza caratteri speciali nella password

                 if (MioConfronto(PassTextBox.Text))
                
                 {
                     i = 5;//variabile per lo switch case
                           //presenza di un carattere speciale
                           //fa questo -> ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('La Password deve contenere almeno un carattere speiale!');", true);
                 }

                //controllo presenza caratteri maiuscoli nella password
                 string prova = "La stringa non contiene Lettere maiuscole";
                 string prova0 = (string)Parse_string_m(PassTextBox.Text);
                 if (prova.Equals(prova0))
                 {
                      i = 6;//variabile per lo switch case
                            //presenza di un carattere maiuscolo
                            //fa questo -> ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('La password non contiene lettere maiuscole');", true);
                 }
                //controllo presenza numeri interi nella password
                 string prova1 = (string)Parse_string(PassTextBox.Text);
                 string prova2 = "La stringa contiene solo Lettere";
                 if (prova1.Equals(prova2))
                 {
                      i = 7;//variabile per lo switch case
                            //presenza di un numero intero
                            //fa questo -> ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('La Password deve contenere almeno un numero intero!');", true);
                 }

             
                 string prova3 = "La stringa contiene solo Numeri";
                 if (prova1.Equals(prova3))//non trovo un metodo adatto al controllo
                 {
                      i = 8;//variabile per lo switch case
                            //presenza di lettere
                            //fa questo -> ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('La Password deve contenere almeno una lettera');", true);
                 }


                 switch (i)
                 {
                
                    case 1:
                    //impostata la nuova password si reindirizza al login
                    //Response.Redirect("About.aspx");
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Cambio Password effettuata con successo!');", true);
                    break;
                    case 2:
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Le password non coincidono!');", true);
                    break;
                    case 3:
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Campo password non settato!');", true);
                    break;
                    case 4:
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('La Password deve contenere almeno 10 caratteri');", true);
                    break;
                    case 5:
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('La Password deve contenere almeno un carattere speciale');", true);
                    break;
                    case 6:
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('La Password deve contenere almeno un carattere maiuscolo');", true);
                    break;
                    case 7:
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('La Password deve contenere almeno un numero intero');", true);
                    break;
                    case 8:
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('La Password deve contenere almeno una lettera');", true);
                    break;
                        
                 }
            }
            else
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('La One Time Password non è corretta');", true);
            }
        }
         
        
    }
}