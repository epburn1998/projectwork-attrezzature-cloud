﻿using System;
using System.Collections.Generic;

public class Utenti
{
	public Utenti()
	{
        public int ID { get; set; }
    public string Nome { get; set; }
    public string Cognome { get; set; }
    public string NomeUtente { get; set; }
    public string Password { get; set; }
    public DateTime EnrollmentDate { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}

	}
}
