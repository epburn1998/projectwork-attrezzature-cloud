﻿using System;
using System.Collections.Generic;

namespace AttrezzatureLibrary
{
    public class Utente
        {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Email { get; set; }
        public string Pass { get; set; }

        }
    }