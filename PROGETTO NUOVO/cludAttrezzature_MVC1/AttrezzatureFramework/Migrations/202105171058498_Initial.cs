namespace AttrezzatureFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attrezzatura",
                c => new
                    {
                        codiceAttrezzatura = c.Int(nullable: false, identity: true),
                        nomeAttrezzatura = c.String(),
                        statoDiFunzionamento = c.String(),
                        categoria = c.String(),
                        dataDiScadenza = c.DateTime(nullable: false),
                        assegnata = c.Boolean(nullable: false),
                        tipoManuale = c.String(),
                        IDFornitore = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.codiceAttrezzatura)
                .ForeignKey("dbo.Fornitore", t => t.IDFornitore)
                .Index(t => t.IDFornitore);
            
            CreateTable(
                "dbo.AttrezzaturaInSquadra",
                c => new
                    {
                        IDAssegnazione = c.Int(nullable: false, identity: true),
                        codiceAttrezzatura = c.Int(nullable: false),
                        IDSquadraAttrezzatura = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IDAssegnazione)
                .ForeignKey("dbo.Attrezzatura", t => t.codiceAttrezzatura)
                .ForeignKey("dbo.Squadra", t => t.IDSquadraAttrezzatura)
                .Index(t => t.codiceAttrezzatura)
                .Index(t => t.IDSquadraAttrezzatura);
            
            CreateTable(
                "dbo.Squadra",
                c => new
                    {
                        IDSquadra = c.Int(nullable: false, identity: true),
                        nomeSquadra = c.String(),
                    })
                .PrimaryKey(t => t.IDSquadra);
            
            CreateTable(
                "dbo.UtenteInSquadra",
                c => new
                    {
                        IDSquadraUtente = c.Int(nullable: false),
                        IDUtente = c.Int(nullable: false),
                        isCaposquadra = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.IDSquadraUtente, t.IDUtente })
                .ForeignKey("dbo.Squadra", t => t.IDSquadraUtente)
                .ForeignKey("dbo.Utente", t => t.IDUtente)
                .Index(t => t.IDSquadraUtente)
                .Index(t => t.IDUtente);
            
            CreateTable(
                "dbo.Utente",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Email = c.String(nullable: false),
                        Nome = c.String(),
                        Cognome = c.String(),
                        Pass = c.String(nullable: false),
                        recuperoPass = c.String(),
                        IDRuolo = c.Int(),
                        IDFornitore = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Fornitore", t => t.IDFornitore)
                .ForeignKey("dbo.Ruolo", t => t.IDRuolo)
                .Index(t => t.IDRuolo)
                .Index(t => t.IDFornitore);
            
            CreateTable(
                "dbo.Fornitore",
                c => new
                    {
                        IDFornitore = c.Int(nullable: false, identity: true),
                        nomeFornitore = c.String(),
                        partitaIVA = c.String(),
                        ragioneSociale = c.String(),
                    })
                .PrimaryKey(t => t.IDFornitore);
            
            CreateTable(
                "dbo.Ruolo",
                c => new
                    {
                        IDRuolo = c.Int(nullable: false, identity: true),
                        descrizione = c.String(),
                    })
                .PrimaryKey(t => t.IDRuolo);
            
            CreateTable(
                "dbo.Audit",
                c => new
                    {
                        IDAudit = c.Int(nullable: false, identity: true),
                        checkVisivoStrumentale = c.Boolean(nullable: false),
                        dataApertura = c.DateTime(nullable: false),
                        dataChiusura = c.DateTime(nullable: false),
                        esitoAudit = c.String(),
                        IDSquadra = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IDAudit)
                .ForeignKey("dbo.Squadra", t => t.IDSquadra)
                .Index(t => t.IDSquadra);
            
            CreateTable(
                "dbo.AuditGestore",
                c => new
                    {
                        IDAudit = c.Int(nullable: false),
                        IDUtente = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IDAudit, t.IDUtente })
                .ForeignKey("dbo.Audit", t => t.IDAudit)
                .ForeignKey("dbo.Utente", t => t.IDUtente)
                .Index(t => t.IDAudit)
                .Index(t => t.IDUtente);
            
            CreateTable(
                "dbo.AuditSquadra",
                c => new
                    {
                        IDAssegnazione = c.Int(nullable: false),
                        IDAudit = c.Int(nullable: false),
                        esitoAuditControllo = c.String(),
                    })
                .PrimaryKey(t => new { t.IDAssegnazione, t.IDAudit })
                .ForeignKey("dbo.AttrezzaturaInSquadra", t => t.IDAssegnazione)
                .ForeignKey("dbo.Audit", t => t.IDAudit)
                .Index(t => t.IDAssegnazione)
                .Index(t => t.IDAudit);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AuditSquadra", "IDAudit", "dbo.Audit");
            DropForeignKey("dbo.AuditSquadra", "IDAssegnazione", "dbo.AttrezzaturaInSquadra");
            DropForeignKey("dbo.AuditGestore", "IDUtente", "dbo.Utente");
            DropForeignKey("dbo.AuditGestore", "IDAudit", "dbo.Audit");
            DropForeignKey("dbo.Audit", "IDSquadra", "dbo.Squadra");
            DropForeignKey("dbo.Attrezzatura", "IDFornitore", "dbo.Fornitore");
            DropForeignKey("dbo.AttrezzaturaInSquadra", "IDSquadraAttrezzatura", "dbo.Squadra");
            DropForeignKey("dbo.UtenteInSquadra", "IDUtente", "dbo.Utente");
            DropForeignKey("dbo.Utente", "IDRuolo", "dbo.Ruolo");
            DropForeignKey("dbo.Utente", "IDFornitore", "dbo.Fornitore");
            DropForeignKey("dbo.UtenteInSquadra", "IDSquadraUtente", "dbo.Squadra");
            DropForeignKey("dbo.AttrezzaturaInSquadra", "codiceAttrezzatura", "dbo.Attrezzatura");
            DropIndex("dbo.AuditSquadra", new[] { "IDAudit" });
            DropIndex("dbo.AuditSquadra", new[] { "IDAssegnazione" });
            DropIndex("dbo.AuditGestore", new[] { "IDUtente" });
            DropIndex("dbo.AuditGestore", new[] { "IDAudit" });
            DropIndex("dbo.Audit", new[] { "IDSquadra" });
            DropIndex("dbo.Utente", new[] { "IDFornitore" });
            DropIndex("dbo.Utente", new[] { "IDRuolo" });
            DropIndex("dbo.UtenteInSquadra", new[] { "IDUtente" });
            DropIndex("dbo.UtenteInSquadra", new[] { "IDSquadraUtente" });
            DropIndex("dbo.AttrezzaturaInSquadra", new[] { "IDSquadraAttrezzatura" });
            DropIndex("dbo.AttrezzaturaInSquadra", new[] { "codiceAttrezzatura" });
            DropIndex("dbo.Attrezzatura", new[] { "IDFornitore" });
            DropTable("dbo.AuditSquadra");
            DropTable("dbo.AuditGestore");
            DropTable("dbo.Audit");
            DropTable("dbo.Ruolo");
            DropTable("dbo.Fornitore");
            DropTable("dbo.Utente");
            DropTable("dbo.UtenteInSquadra");
            DropTable("dbo.Squadra");
            DropTable("dbo.AttrezzaturaInSquadra");
            DropTable("dbo.Attrezzatura");
        }
    }
}
