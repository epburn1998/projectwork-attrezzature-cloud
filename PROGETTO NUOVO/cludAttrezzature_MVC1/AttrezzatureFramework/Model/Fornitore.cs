﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace AttrezzatureFramework
{
    public class Fornitore
    {
        [Key]
        //[MaxValue(100)]
        [Display(Name = "Nome fornitore")]
        public int IDFornitore { get; set; }

        [Display(Name = "Nome Fornitore")]
        public string nomeFornitore { get; set; }
        //[MaxLength(50)]
        [Display(Name = "Partita IVA")]
        public string partitaIVA { get; set; }
        // [MaxLength(50)]
        [Display(Name = "Ragione sociale")]
        public string ragioneSociale { get; set; }
    }
}
