﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AttrezzatureFramework
{
    public class Utente
    {
        [Key]
        //[MaxValue(50)]
        public int ID { get; set; }
        // [MaxLength(10)]
        [Required(ErrorMessage = "Campo obbligatorio")]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Email non valida")]
        public string Email { get; set; }
        // [MaxLength(10)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }
        // [MaxLength(50)]
        [Display(Name = "Cognome")]
        public string Cognome { get; set; }

        //non settiamo un valore massimo per la password per motivi di security
        [Required(ErrorMessage = "Campo obbligatorio")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Pass { get; set; }

        public string recuperoPass { get; set; }


        [ForeignKey("Ruolo")]
        //[MaxValue(10)]
        public int? IDRuolo { get; set; }
        public Ruolo Ruolo { get; set; }





        [ForeignKey("Fornitore")]
        //[MaxValue(10)]
        public int? IDFornitore { get; set; }
        public Fornitore Fornitore { get; set; }
    }

    public class UtenteDto
    {
        

        public int ID { get; set; }
        public int? IDSquadra { get; set; }
        public bool? isCaposquadra { get; set; }
        public string emailUtente { get; set; }
        public string nomeUtente { get; set; }
        public string cognomeUtente { get; set; }

        public string passUtente { get; set; }

        public string ruoloUtente { get; set; }
    }

    public class UtenteDetailsDTO
    {
        public int ID { get; set; }
        public int IDSquadra { get; set; }
        public bool isCaposquadra { get; set; }
        public string emailUtente { get; set; }
        public string nomeUtente { get; set; }
        public string cognomeUtente { get; set; }
    
        public string passUtente { get; set; }

        public string ruoloUtente { get; set; }

        
    }


   
}