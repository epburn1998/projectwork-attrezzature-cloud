﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework
{
    
    public class Squadra
    {
        [Key]
        //[MaxValue(100)]
        public int IDSquadra { get; set; }

       [Display(Name ="Nome Squadra")]
       public string nomeSquadra { get; set; }

        [Display(Name ="Utenti in Squadra")]
       public virtual ICollection<UtenteInSquadra> listaUtentiInSquadra { get; set; }

        [Display(Name = "Attrezzature in Squadra")]
        public virtual ICollection<AttrezzaturaInSquadra> listaAttrezzatureInSquadra { get; set; }


    }

    public class SquadraDTO
    {
        public int IDSquadra { get; set; }
        public string nomeSquadra { get; set; }
    }
}
