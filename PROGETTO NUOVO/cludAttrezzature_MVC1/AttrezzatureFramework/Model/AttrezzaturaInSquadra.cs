﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework
{
    public class AttrezzaturaInSquadra
    {
        [Key]
        public int IDAssegnazione { get; set; }

        [Display(Name = "Data assegnazione attrezzatura")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime dataAssegnazione { get; set; }

        [ForeignKey("Attrezzatura")]
        public int codiceAttrezzatura { get; set; }
        public Attrezzatura Attrezzatura { get; set; }

        [ForeignKey("Squadra")]
        public int IDSquadraAttrezzatura { get; set; }
        public Squadra Squadra { get; set; }


    }

    public class AttrezzaturaInSquadraDTO
    {
        public int IDAssegnazione { get; set; }
        public int codiceAttrezzatura { get; set; }
        public int IDSquadra { get; set; }
    }

    public class attSquadCambio
    {
        public int idAssegnazione { get; set; }
        public int codiceAttrezzatura { get; set; }
        public int IDSquadra { get; set; }
    }
}
