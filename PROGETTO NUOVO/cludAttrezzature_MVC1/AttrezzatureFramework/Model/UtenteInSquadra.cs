﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework
{
    public class UtenteInSquadra
    {
        //[MaxLength(20)]
        public bool isCaposquadra { get; set; }

        [Key, Column(Order = 0)]
        [ForeignKey("Squadra")]
        //[MaxValue(10)]
        public int IDSquadraUtente { get; set; }
        public Squadra Squadra{ get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Utente")]
        //[MaxValue(100)]
        public int IDUtente { get; set; }
        public Utente Utente { get; set; }


    }

    public class UtenteInSquadraDTO
    {
        public int IDUtente { get; set; }
        public int IDSquadra { get; set; }
        public bool isCaposquadra{ get; set; }
    }
}
