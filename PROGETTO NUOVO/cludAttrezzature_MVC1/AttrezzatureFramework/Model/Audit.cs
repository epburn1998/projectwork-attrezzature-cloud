﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework
{
    public class Audit
    {
        [Key]
        //[MaxValue(100)]
        public int IDAudit { get; set; }
        // [MaxLength(2)][MaxValue(2)]
        [Display(Name = "Controllo Visivo/Strumentale")]
        public bool checkVisivoStrumentale { get; set; }
        //[MaxLength(8)][MaxValue(8)]
        [Display(Name ="Data di apertura")]
        [Column(TypeName ="datetime2")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime dataApertura { get; set; }
        // [MaxLength(8)][MaxValue(8)]
        [Display(Name = "Data di chiusura")]
        [Column(TypeName = "datetime2")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime dataChiusura { get; set; }
        // [MaxLength(15)]
        [Display(Name = "Esito Audit")]
        public Esito? esitoAudit { get; set; }

        [ForeignKey("Squadra")]
        [Display(Name = "Squadra")]
        public int IDSquadra { get; set; }
        public Squadra Squadra { get; set; }
    }

    public enum Esito
    {
        Attesa,    
        Scaduto,
        Concluso
    }

    public class AuditDTO
    {
        public int IDAudit { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime dataApertura { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime dataChiusura { get; set; }
        public string esitoAudit { get; set; }

    }

    public class AuditPutDTO
    {
        public int idAudit { get; set; }
        public int idAssegnazione { get; set; }
        public int codiceAttrezzatura { get; set; }
        public string nomeAttrezzatura { get; set; }
        public string esitoAttrezzatura { get; set; }
        public bool checkVisivoStrumentale { get; set; }
        public string statoAttrezzatura { get; set; }
        public string descrizioneUtilizzo { get; set; }
        public string noteAggiuntive { get; set; }
    }
}
