﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework
{
    public class AuditGestore
    {
        [Key,Column(Order=0)]
        [ForeignKey("Audit")]
        public int IDAudit { get; set; }
        public Audit Audit { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Utente")]
        public int IDUtente { get; set; }
        public Utente Utente { get; set; }
    }
}
