﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework
{
    public class Attrezzatura
    {
        [Key]

        //[MaxValue(100)]
        [Display(Name = "Codice attrezzatura")]
        public int codiceAttrezzatura { get; set; }
        [Display(Name = "Nome attrezzatura")]
        public string nomeAttrezzatura { get; set; }
        //[MaxLength(20)]
        [Display(Name = "Stato di funzionamento")]
        public string statoDiFunzionamento { get; set; }
        //[MaxLength(20)]
        [Display(Name = "Categoria")]
        public string categoria { get; set; }
        //[MaxLength(8)][MaxValue(8)]
        [Display(Name = "Data scadenza manutenzione")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime dataDiScadenza { get; set; }
        //[MaxLength(2)][MaxValue(2)]
        [Display(Name = "Attualmente assegnata")]
        public bool assegnata { get; set; }
        //[MaxLength(20)]
        [Display(Name = "Tipo di manuale")]
        public string tipoManuale { get; set; }

        [Display(Name = "Nome Fornitore")]
        [ForeignKey("Fornitore")]
        public int IDFornitore { get; set; }
        public Fornitore Fornitore { get; set; }

        public ICollection<AttrezzaturaInSquadra> AttrezzatureInSquadra { get; set; }

    }

    public class AttrezzaturaDTO
    {
        public int codiceAttrezzatura { get; set; }
        public string nomeAttrezzatura { get; set; }
        public string statoAttrezzatura { get; set; }
    }
        

}
