﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework
{
    public class AuditSquadra
    {
       
       [Key, Column(Order = 0)]
       [ForeignKey("AttrezzaturaInSquadra")]
       public int IDAssegnazione { get; set; }
       public AttrezzaturaInSquadra AttrezzaturaInSquadra { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Audit")]
        public int IDAudit { get; set; }
        public virtual Audit Audit { get; set; }
       // [MaxLength(15)]
        public Esito? esitoAuditControllo { get; set; }
        public string descrizioneUtilizzo { get; set; }
        public string noteAggiuntive { get; set; }
    }

    public class AuditSquadraDTO
    {
        public int IDAssegnazione { get; set; }
        public int IDAudit { get; set; }
        public int IDSquadra { get; set; }
        public string esitoAuditControllo { get; set; } //Messo a stringa perché per qualche motivo l'XML non stampa gli enum
        public bool checkVisivoStrumentale { get; set; } //se vero, quando si accede all'attrezzatura dall'app, si dovrá scannerizzare il QR Code

   
    }
}
