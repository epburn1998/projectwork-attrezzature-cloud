﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework.VM
{
    public class EsitoAuditVM
    {
        public List<AuditSquadra> auditSquadre { get; set; }
        public List<AuditGestore> auditGestore { get; set; }

        public List<Attrezzatura> attrezzature { get; set; }
        public List<AttrezzaturaInSquadra> attrezzaturaInSquadras { get; set; }
        public List<Squadra> squadras { get; set; }
        public List<Audit> audits { get; set; }

        public Esito? esiti { get; set; }
    }
}
