﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework.VM
{
    public class UtenteInSquadraVM
    {
        public UtenteInSquadraVM()
        {
            


        }
        public Utente utente { get; set; }

        public int IDutente { get; set; }

        public int IDSquadra { get; set; }

        public bool IsCaposquadra { get; set; }
    }
}
