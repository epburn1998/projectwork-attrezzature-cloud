﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework.VM
{
    public class OperaiSquadraVM
    {
        
        public ICollection<UtenteInSquadraVM> listaUtenti { get; set; }
        public int IDUtente { get; set; }
       
        public ICollection<UtenteInSquadra> utentiInSquadra { get; set; }
        
        public Squadra squadra { get; set; }

        


    }
}

