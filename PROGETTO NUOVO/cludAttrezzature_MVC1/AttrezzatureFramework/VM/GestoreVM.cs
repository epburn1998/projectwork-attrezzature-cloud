﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework.VM
{
    public class GestoreVM
    {
        public List<Utente> Utenti { get; set; }
        public List<Ruolo> Ruoli { get; set; }
    }
}
