﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework.VM
{
    public class AuditVM
    {
        public Audit audit { get; set; }
        public AuditSquadra auditSquadra {get;set;}

        [Required(ErrorMessage = "Scegli una Squadra!")]
        public int? selectedSquadra { get; set; }
        [Required(ErrorMessage = "Scegli una attrezzatura assegnata!")]
        public int? selectedAttrezzaturaInSquadra { get; set; }

        [Display(Name="Attrezzature in Squadra")]
        public List<AttrezzaturaInSquadra> attrezzaturaInSquadra { get; set; }
    }
}
