﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework.VM
{
    public class AttrezzaturaSquadraVM
    {
        public ICollection<AttrezzaturaInSquadraVM> listaAttrezzatura { get; set; }
        public int codiceAttrezzatura { get; set; }

        public ICollection<AttrezzaturaInSquadra> attrezzaturaInSquadra { get; set; }

        public Squadra squadra { get; set; }
    }
}
