﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework.VM
{
    public class CensimentoAttrezzaturaVM
    {
        public Attrezzatura attrezzatura { get; set; }
        public List<Fornitore> listaFornitori { get; set; }
    }
}
