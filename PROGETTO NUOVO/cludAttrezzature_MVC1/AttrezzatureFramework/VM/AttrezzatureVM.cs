﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttrezzatureFramework.VM
{
    public class AttrezzatureVM
    {
        public List<Attrezzatura> Attrezzature { get; set; }
        public List<AttrezzaturaInSquadra> AttrezzaturaInSquadras { get; set; }

        public List<Squadra> Squadre { get; set; }
        public List<Fornitore> Fornitori { get; set; }
        
        


    }
}
