﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows;
using System.Web;
using System.Security.Cryptography;//using per la cifratura


namespace AttrezzatureFramework.DAL
{
    public class Cifrario
    {
        //come faccio a non inserire la chiave di cifratura in chiaro nel codice?
        private string chiave = "aswqdfre1324fgtr5764jkiu8769lo90";//32 caratteri chiave di crittografia
        private string vettore = "qqqwwe34hj789kii";//16 caratteri


        //Metodo per criptare User:
        public string Cript(string p)
        {
            //prende in input una stringa in chiaro

            byte[] PasswordDaCriptare = ASCIIEncoding.ASCII.GetBytes(p);
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;//bit
            aes.KeySize = 256;//caratteri chiave provata
            aes.Key = ASCIIEncoding.ASCII.GetBytes(chiave);
            aes.IV = ASCIIEncoding.ASCII.GetBytes(vettore);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            ICryptoTransform ict = aes.CreateEncryptor(aes.Key, aes.IV);
            byte[] testocriptato = ict.TransformFinalBlock(PasswordDaCriptare, 0, PasswordDaCriptare.Length);
            ict.Dispose();

            string s = Convert.ToBase64String(testocriptato);

            return s;
            //restituisce la stringa criptata
        }
        //Metodo per cifrare la password q: funzione hash
        public string CriptDue(string q)//password
        {
            string source = q;
            using (SHA256 sha256Hash = SHA256.Create())
            {
                string hash = GetHash(sha256Hash, source);

                return hash;
                //restituisce la stringa cifrata con la funzione hash
            }

        }

        private static string GetHash(HashAlgorithm hashAlgorithm, string input)
        {

            // Converte input string in un byte array ed esegue l'hash.
            byte[] data = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(input));

            // crea la stringa
            var sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            //ritorna la stringa 
            return sBuilder.ToString();
        }


        // Metodo per la Verifica
        /*
            private static bool VerifyHash(HashAlgorithm hashAlgorithm, string input, string hash)
            {
              // Hash input
              var hashOfInput = GetHash(hashAlgorithm, input);

              // Compara 
              StringComparer compara = StringComparer.OrdinalIgnoreCase;

              return compara.Compare(hashOfInput, hash) == 0;

              if (VerifyHash(sha256Hash, source, hash))
                {
                    //Console.WriteLine("La stringa è la stessa.");
                }
                else
                {
                    //Console.WriteLine("La stringa non è la stessa.");
                }
            }*/

    }
}
