﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MimeKit;
using MailKit.Net.Smtp;

namespace AttrezzatureFramework.Utils
{
    public class SendEmail
    {
        public void sendEmail(string email, string subject, string body)
        {
            //
            //
            // QUESTA FUNZIONE NON FA NIENTE AL MOMENTO
            // E NON LO FARÁ FINCHÉ NON ABBIAMO UN INDIRIZZO EMAIL DA DOVE MANDARE
            // I MESSAGGI. CON QUESTO DOVREMMO PARLARE CON I PROF A RIGUARDO
            //
            //

            MimeMessage messageM = new MimeMessage();
            messageM.From.Add(new MailboxAddress("from", "email@prova.it"));  //Indirizzo mittente !!!!!DA SOSTITUIRE CON L'INDIRIZZO AZIENDALE!!!!!!!!!!!!
            messageM.To.Add(new MailboxAddress("to", email));        //Destinatario (email da recuperare)
            messageM.Subject = subject;
            messageM.Body = new TextPart("plain")
            {
                Text = body
            };

            using (var client = new SmtpClient())
            {
                //Demo
                client.ServerCertificateValidationCallback = (s, c, h, ev) => true;

                client.Connect("smtp.live.com", 587); //SMTP su dove ci andiamo a connettere--- DIPENDE DA QUALE INDIRIZZO ANDREMO A CREARE

                client.Authenticate("email", "password");  //Da sosittuire con l'email con cui ci connettiamo all'SMTP ( e password)

                try
                {

                    client.Send(messageM);
                    client.Disconnect(true);

                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc.ToString());
                }
            }
        }
    }
}
