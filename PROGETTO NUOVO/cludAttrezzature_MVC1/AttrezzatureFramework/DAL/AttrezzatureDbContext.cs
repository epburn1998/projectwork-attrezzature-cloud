﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace AttrezzatureFramework.DAL
{
    public class AttrezzatureDbContext : DbContext
    {
        public AttrezzatureDbContext() : base("AttrezzatureDbContext")
        {
        }

        public IDbSet<Utente> Utenti { get; set; }
        public IDbSet<Squadra> Squadre { get; set; }
        public IDbSet<Attrezzatura> Attrezzatura { get; set; }
        public IDbSet<Audit> Audits { get; set; }
        public IDbSet<UtenteInSquadra> UtentiInSquadra { get; set; }
        public IDbSet<AttrezzaturaInSquadra> attrezzatureInSquadra { get; set; }
        public IDbSet<AuditGestore> AuditsGestore { get; set; }
        public IDbSet<AuditSquadra> AuditsSquadra { get; set; }
        public IDbSet<Ruolo> Ruoli { get; set; }
        public IDbSet<Fornitore> Fornitori { get; set; }




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToOneConstraintIntroductionConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        
    }
}