﻿using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AttrezzatureFramework;
using AttrezzatureFramework.DAL;


namespace cludAttrezzature_MVC1.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AuditAPIController : ApiController
    {
        private AttrezzatureDbContext db = new AttrezzatureDbContext();

        // GET: api/AuditAPI
        public IQueryable<AuditDTO> GetAudits()
        {
            var audit = from b in db.Audits select new AuditDTO()
            {
                IDAudit = b.IDAudit,
                dataApertura = b.dataApertura,
                dataChiusura = b.dataChiusura,
                esitoAudit = b.esitoAudit.ToString()
            };
            return audit;
        }

        // GET: api/AuditAPI/5
        [ResponseType(typeof(AuditDTO))]
        public async Task<IHttpActionResult> GetAudit(int id)
        {
            var audit = await db.Audits.Select(b => new AuditDTO()
            {
                IDAudit = b.IDAudit,
                dataApertura = b.dataApertura,
                dataChiusura = b.dataChiusura,
                esitoAudit = b.esitoAudit.ToString()
            }).SingleOrDefaultAsync(b => b.IDAudit == id);
            if (audit == null)
            {
                return NotFound();
            }

            return Ok(audit);
        }

        // PUT: api/AuditAPI/5
        
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAudit(int id, AuditPutDTO[] listaAttAudits)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            foreach(var e in listaAttAudits)
            {
                if (id != e.idAudit)
                {
                    return BadRequest();
                }
            }
            
            //da elaborare per bene e sostituire
            //db.Entry(listaAttAudits).State = EntityState.Modified;
            foreach(var aud in listaAttAudits)
            {
                db.Attrezzatura.Where(x => x.codiceAttrezzatura == aud.codiceAttrezzatura)
                    .FirstOrDefault().statoDiFunzionamento = aud.statoAttrezzatura;
                db.AuditsSquadra.Where(x => x.IDAssegnazione == aud.idAssegnazione )
                    .FirstOrDefault().descrizioneUtilizzo = aud.descrizioneUtilizzo;
                db.AuditsSquadra.Where(x => x.IDAssegnazione == aud.idAssegnazione)
                    .FirstOrDefault().noteAggiuntive = aud.noteAggiuntive;
                db.AuditsSquadra.Where(x => x.IDAssegnazione == aud.idAssegnazione)
                    .FirstOrDefault().esitoAuditControllo = Esito.Concluso;

            }

            db.Audits.Where(x => x.IDAudit == id).FirstOrDefault()
                .esitoAudit = Esito.Concluso;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AuditExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AuditAPI
        [ResponseType(typeof(Audit))]
        public async Task<IHttpActionResult> PostAudit(Audit audit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Audits.Add(audit);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = audit.IDAudit }, audit);
        }

        // DELETE: api/AuditAPI/5
        [ResponseType(typeof(AuditDTO))]
        public async Task<IHttpActionResult> DeleteAudit(int id)
        {
            Audit audit = db.Audits.Find(id);
            if (audit == null)
            {
                return NotFound();
            }

            db.Audits.Remove(audit);
            await db.SaveChangesAsync();

            return Ok(audit);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AuditExists(int id)
        {
            return db.Audits.Count(e => e.IDAudit == id) > 0;
        }
    }
}