﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AttrezzatureFramework;
using AttrezzatureFramework.DAL;

namespace cludAttrezzature_MVC1.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AuditSquadraAPIController : ApiController
    {
        private AttrezzatureDbContext db = new AttrezzatureDbContext();

        // GET: api/AuditSquadras
        public IQueryable<AuditSquadraDTO> GetAuditSquadras()
        {
            var auditSquadra = from b in db.AuditsSquadra
                               select new AuditSquadraDTO()
                               {
                                   IDAudit = b.IDAudit,
                                   IDAssegnazione = b.IDAssegnazione,
                                   IDSquadra = db.attrezzatureInSquadra.Where(a=>a.IDAssegnazione==b.IDAssegnazione).FirstOrDefault().IDSquadraAttrezzatura,
                                   esitoAuditControllo = b.esitoAuditControllo.ToString(),
                                   checkVisivoStrumentale = db.Audits.Where(a=>a.IDAudit == b.IDAudit).FirstOrDefault().checkVisivoStrumentale
                               };
            return auditSquadra;
        }

        // GET: api/AuditSquadras/5
        [ResponseType(typeof(AuditSquadraDTO))]
        public async Task<IHttpActionResult> GetAuditSquadra(int id)
        {
            var auditSquadra = await db.AuditsSquadra.Select(b => new AuditSquadraDTO()
            {
                IDAudit = b.IDAudit,
                IDAssegnazione = b.IDAssegnazione,
                IDSquadra = db.attrezzatureInSquadra.Where(a => a.IDAssegnazione == b.IDAssegnazione).FirstOrDefault().IDSquadraAttrezzatura,
                esitoAuditControllo = b.esitoAuditControllo.ToString(),
                checkVisivoStrumentale = db.Audits.Find(b.IDAudit).checkVisivoStrumentale
            }).SingleOrDefaultAsync(b => b.IDAudit == id);
            if (auditSquadra == null)
            {
                return NotFound();
            }

            return Ok(auditSquadra);
        }

        // PUT: api/AuditSquadras/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAuditSquadra(int id, AuditSquadra auditSquadra)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != auditSquadra.IDAssegnazione)
            {
                return BadRequest();
            }

            db.Entry(auditSquadra).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AuditSquadraExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AuditSquadras
        [ResponseType(typeof(AuditSquadra))]
        public async Task<IHttpActionResult> PostAuditSquadra(AuditSquadra auditSquadra)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AuditsSquadra.Add(auditSquadra);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AuditSquadraExists(auditSquadra.IDAssegnazione))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = auditSquadra.IDAssegnazione }, auditSquadra);
        }

        // DELETE: api/AuditSquadras/5
        [ResponseType(typeof(AuditSquadra))]
        public async Task<IHttpActionResult> DeleteAuditSquadra(int id)
        {
            AuditSquadra auditSquadra =  db.AuditsSquadra.Find(id);
            if (auditSquadra == null)
            {
                return NotFound();
            }

            db.AuditsSquadra.Remove(auditSquadra);
            await db.SaveChangesAsync();

            return Ok(auditSquadra);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AuditSquadraExists(int id)
        {
            return db.AuditsSquadra.Count(e => e.IDAssegnazione == id) > 0;
        }
    }
}