﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AttrezzatureFramework;
using AttrezzatureFramework.DAL;

namespace cludAttrezzature_MVC1.Controllers
{
    public class RuolosController : Controller
    {
        private AttrezzatureDbContext db = new AttrezzatureDbContext();

        // GET: Ruolos
        public ActionResult Index()
        {
            return View(db.Ruoli.ToList());
        }

        // GET: Ruolos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ruolo ruolo = db.Ruoli.Find(id);
            if (ruolo == null)
            {
                return HttpNotFound();
            }
            return View(ruolo);
        }

        // GET: Ruolos/Create
        public ActionResult AggiungiRuolo()
        {
            return View();
        }

        // POST: Ruolos/Create
        // Per la protezione da attacchi di overposting, abilitare le proprietà a cui eseguire il binding. 
        // Per altri dettagli, vedere https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AggiungiRuolo([Bind(Include = "IDRuolo,descrizione")] Ruolo ruolo)
        {
            if (ModelState.IsValid)
            {
                db.Ruoli.Add(ruolo);
                db.SaveChanges();
                return RedirectToAction("Utente/GestioneRuoli");
            }

            return View(ruolo);
        }

        // GET: Ruolos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ruolo ruolo = db.Ruoli.Find(id);
            if (ruolo == null)
            {
                return HttpNotFound();
            }
            return View(ruolo);
        }

        // POST: Ruolos/Edit/5
        // Per la protezione da attacchi di overposting, abilitare le proprietà a cui eseguire il binding. 
        // Per altri dettagli, vedere https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDRuolo,descrizione")] Ruolo ruolo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ruolo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ruolo);
        }

        // GET: Ruolos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ruolo ruolo = db.Ruoli.Find(id);
            if (ruolo == null)
            {
                return HttpNotFound();
            }
            return View(ruolo);
        }

        // POST: Ruolos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ruolo ruolo = db.Ruoli.Find(id);
            db.Ruoli.Remove(ruolo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
