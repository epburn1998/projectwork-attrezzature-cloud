﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AttrezzatureFramework;
using AttrezzatureFramework.DAL;


namespace cludAttrezzature_MVC1.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UtenteAPIController : ApiController
    {
        private AttrezzatureDbContext db = new AttrezzatureDbContext();

        // GET: api/UtenteAPI
        public IQueryable<UtenteDto> GetUtentes()
        {
            var IDSquadra = db.UtentiInSquadra.Where(x => x.IDUtente == 1)
                                .FirstOrDefault().IDSquadraUtente;
            var utenti = from b in db.Utenti
                        select new UtenteDto()
                        {
                            ID = b.ID,
                            emailUtente = b.Email,
                            passUtente = b.Pass,
                            IDSquadra = db.UtentiInSquadra.Where(x => x.IDUtente == b.ID)
                                .FirstOrDefault().IDSquadraUtente,
                            isCaposquadra = db.UtentiInSquadra.Where(x => x.IDUtente == b.ID)
                                .FirstOrDefault().isCaposquadra,
                            nomeUtente = b.Nome,
                            cognomeUtente = b.Cognome,
                            ruoloUtente = db.Ruoli.Where(x=>x.IDRuolo == b.IDRuolo).FirstOrDefault().descrizione
                        };
            return utenti;
        }


        // GET: api/UtenteAPI/5
        [ResponseType(typeof(UtenteDetailsDTO))]
        public async Task<IHttpActionResult> GetUtente(int id)
        {
            var utente= await db.Utenti.Select(b => new UtenteDetailsDTO()
            {
                ID = b.ID,
                IDSquadra = db.UtentiInSquadra.Where(x=>x.IDUtente==b.ID)
                .FirstOrDefault().IDSquadraUtente,
                isCaposquadra = db.UtentiInSquadra.Where(x => x.IDUtente == b.ID)
                .FirstOrDefault().isCaposquadra,
                emailUtente = b.Email,
                nomeUtente = b.Nome,
                cognomeUtente = b.Cognome,
                passUtente = b.Pass, //DA CRITOGRAFARE!!!!!,
                ruoloUtente = db.Ruoli.Where(x => x.IDRuolo == b.IDRuolo).FirstOrDefault().descrizione
            }).SingleOrDefaultAsync(b => b.ID == id);
            if (utente == null)
            {
                return NotFound();
            }

            return Ok(utente);
        }

        // PUT: api/UtenteAPI/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutUtente(int id, Utente utente)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != utente.ID)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(utente).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!UtenteExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        // POST: api/UtenteAPI
        [ResponseType(typeof(UtenteDetailsDTO))]
        public async Task<IHttpActionResult> PostUtente(UtenteDetailsDTO utenteDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var utente = new Utente()
            {
                Nome = utenteDTO.nomeUtente,
                Cognome = utenteDTO.cognomeUtente,
                Email = utenteDTO.emailUtente,
                Pass = utenteDTO.passUtente
            };

            db.Utenti.Add(utente);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = utente.ID }, utente);
        }

        // DELETE: api/UtenteAPI/5
        [ResponseType(typeof(Utente))]
        public IHttpActionResult DeleteUtente(int id)
        {
            Utente utente = db.Utenti.Find(id);
            if (utente == null)
            {
                return NotFound();
            }

            db.Utenti.Remove(utente);
            db.SaveChanges();

            return Ok(utente);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UtenteExists(int id)
        {
            return db.Utenti.Count(e => e.ID == id) > 0;
        }
    }
}