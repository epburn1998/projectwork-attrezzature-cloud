﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AttrezzatureFramework;
using AttrezzatureFramework.DAL;

namespace cludAttrezzature_MVC1.Views.Utente
{
    public class AttrezzaturasController : Controller
    {
        private AttrezzatureDbContext db = new AttrezzatureDbContext();

        // GET: Attrezzaturas
        public ActionResult GestioneAttrezzature()
        {
            return View(db.Attrezzatura.ToList());
        }

        // GET: Attrezzaturas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attrezzatura attrezzatura = db.Attrezzatura.Find(id);
            if (attrezzatura == null)
            {
                return HttpNotFound();
            }
            return View(attrezzatura);
        }

        // GET: Attrezzaturas/Create
        public ActionResult Censimento()
        {
            return View();
        }

        // POST: Attrezzaturas/Create
        // Per la protezione da attacchi di overposting, abilitare le proprietà a cui eseguire il binding. 
        // Per altri dettagli, vedere https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Censimento([Bind(Include = "codiceAttrezzatura,statoDiFunzionamento,categoria,dataDiScadenza,assegnata,tipoManuale")] Attrezzatura attrezzatura)
        {
            if (ModelState.IsValid)
            {
                db.Attrezzatura.Add(attrezzatura);
                db.SaveChanges();
                return RedirectToAction("GestioneAttrezzature");
            }

            return View(attrezzatura);
        }

        // GET: Attrezzaturas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attrezzatura attrezzatura = db.Attrezzatura.Find(id);
            if (attrezzatura == null)
            {
                return HttpNotFound();
            }
            return View(attrezzatura);
        }

        // POST: Attrezzaturas/Edit/5
        // Per la protezione da attacchi di overposting, abilitare le proprietà a cui eseguire il binding. 
        // Per altri dettagli, vedere https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "codiceAttrezzatura,statoDiFunzionamento,categoria,dataDiScadenza,assegnata,tipoManuale")] Attrezzatura attrezzatura)
        {
            if (ModelState.IsValid)
            {
                db.Entry(attrezzatura).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(attrezzatura);
        }

        // GET: Attrezzaturas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attrezzatura attrezzatura = db.Attrezzatura.Find(id);
            if (attrezzatura == null)
            {
                return HttpNotFound();
            }
            return View(attrezzatura);
        }

        // POST: Attrezzaturas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Attrezzatura attrezzatura = db.Attrezzatura.Find(id);
            db.Attrezzatura.Remove(attrezzatura);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
