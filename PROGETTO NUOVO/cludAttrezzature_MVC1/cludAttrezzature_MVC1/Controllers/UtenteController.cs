﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttrezzatureFramework;
using AttrezzatureFramework.DAL;
using AttrezzatureFramework.Model;
using AttrezzatureFramework.VM;
using AttrezzatureFramework.Utils;
using System.Threading.Tasks;
using System.Data.Entity;

namespace cludAttrezzature_MVC1.Controllers
{
    public class UtenteController : Controller
    {
        private AttrezzatureDbContext db = new AttrezzatureDbContext();
        // GET: Utente
        public ActionResult Index()
        {
            try
            {
                if (Session["ID"] != null)
                {
                    return RedirectToAction("HomepageLogged");
                }
                else
                {
                    return View();
                }
            }
            catch (NullReferenceException ex)
            {
                return RedirectToAction("Index");
            }
            
        }

        public ActionResult AggiungiRuolo()
        {
            try
            {
                if (Session["UserRole"].Equals(1))
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (NullReferenceException ex)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        
        public ActionResult AggiungiRuolo(Ruolo ruolo)
        {
            if (ModelState.IsValid)
            {
                db.Ruoli.Add(ruolo);
                db.SaveChanges();
                return RedirectToAction("GestioneRuoli");
            }
            else
            {
                ModelState.AddModelError("", "Qualcosa é andato storto");
            }
            return View(ruolo);
        }




        public ActionResult Censimento()
        {
            CensimentoAttrezzaturaVM model = new CensimentoAttrezzaturaVM()
            {
                attrezzatura = new Attrezzatura(),
                listaFornitori = db.Fornitori.ToList()
            };

            model.attrezzatura.IDFornitore = 1;
            model.attrezzatura.dataDiScadenza = DateTime.Now;

            try
            {
                if (Session["UserRole"].Equals(1))
                {
                    return View(model);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (NullReferenceException ex)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult Censimento(CensimentoAttrezzaturaVM model)
        {
            
            if (ModelState.IsValid)
            {
                db.Attrezzatura.Add(model.attrezzatura);
                db.SaveChanges();
                return RedirectToAction("ElencoAttrezzature");
            }
            else
            {
                ModelState.AddModelError("", "Qualcosa é andato storto");
            }
            return View(model);
        }

        public ActionResult Login()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Utente utente)
        {
            if (ModelState.IsValid)
            {
                using (AttrezzatureDbContext db =new AttrezzatureDbContext())
                {
                    var obj = db.Utenti.Where(u => u.Email.Equals(utente.Email) && u.Pass.Equals(utente.Pass)).FirstOrDefault();
                    if (obj != null)
                    {
                        Session["ID"] = obj.ID.ToString();
                        Session["Email"] = obj.Email.ToString();
                        Session["UserRole"] = obj.IDRuolo;
                        Session["Nome"] = obj.Nome;
                        return RedirectToAction("HomepageLogged");
                    }
                
                }
            }
            return View(utente);
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Utente");
        }

        

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(Utente utente)
        {
            if (ModelState.IsValid)
            {
                db.Utenti.Add(utente);
                db.SaveChanges();
                return RedirectToAction("Login");
            }
            else
            {
                ModelState.AddModelError("","Qualcosa é andato storto");
            }
            return View(utente);
        }

        public ActionResult HomepageLogged()
        {
            if (Session["ID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(string EmailID)
        {
            string resetCode = Guid.NewGuid().ToString();
            var verifyURL = "Utente/ForgotPassword" + resetCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyURL);

            var obj = db.Utenti.Where(u => u.Email.Equals(EmailID)).FirstOrDefault();
            if (obj != null)
            {
                obj.recuperoPass = resetCode;

                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();

                var subject = "Richiesta di ripristino password";
                var body = "Ciao " + obj.Nome + ", <br/> di recente hai richiesto di resettare la tua password, clicca il link per andare alla pagina di ripristino password " +

                     " <br/><br/><a href='" + link + "'>" + link + "</a> <br/><br/>" +
                     "Se non hai richiesto niente, ignora questa email e faccelo sapere. <br/><br/> Grazie mille.";

                SendEmail sender = new SendEmail();
                sender.sendEmail(obj.Email, subject, body);

                ViewBag.Message = "Abbiamo inviato una email contenente il link di recupero della password";
            }
            else
            {
                ViewBag.Message = "Non esiste nessun utente con questo indirizzo nel nostro database.";
                return View();
            }
            return View();
        }

        public ActionResult ResetPassword(string id)
        {
            //Verify the reset password link
            //Find account associated with this link
            //redirect to reset password page
            if (string.IsNullOrWhiteSpace(id))
            {
                return HttpNotFound();
            }

            var utente = db.Utenti.Where(a => a.recuperoPass == id).FirstOrDefault();
            
                if (utente != null)
                {
                    ResetPasswordModel model = new ResetPasswordModel();
                    model.ResetCode = id;
                    return View(model);
                }
                else
                {
                    return HttpNotFound();
                }
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            var message = "";
            if (ModelState.IsValid)
            {
                
                    var user = db.Utenti.Where(a => a.recuperoPass == model.ResetCode).FirstOrDefault();
                    if (user != null)
                    {
                        //you can encrypt password here, we are not doing it
                        user.Pass = model.NewPassword;
                        //make resetpasswordcode empty string now
                        user.recuperoPass = "";
                        //to avoid validation issues, disable it
                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        message = "La password é stata cambiata con successo!";
                    }
                
            }
            else
            {
                message = "Qualcosa é andato storto...";
            }
            ViewBag.Message = message;
            return View(model);
        }
        
        //GET
        public ActionResult AggiuntaSquadra()
        {
            try
            {
                if (Session["ID"] == null)
                {
                    return RedirectToAction("Login");
                }
                if (Session["UserRole"].Equals(1))
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("HomepageLogged");
                }
            }
            catch (NullReferenceException ex)
            {
                return RedirectToAction("Index");
            }
            Squadra squadra = new Squadra();

            ViewBag.ListaUtenti = new SelectList(db.Utenti, "ID", "Cognome", "--Seleziona--");
            ViewBag.ListaAttrezzature = new SelectList(db.Attrezzatura, "codiceAttrezzatura", "tipoManuale", "--Seleziona--");
            return View();
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AggiuntaSquadra(Squadra squadra)
        {
         

            db.Squadre.Add(squadra);
            db.SaveChangesAsync();

            return RedirectToAction("HomepageLogged");
        }

        //GET
        public ActionResult GestioneSquadra(int id)
        {
            try
            {
                if(Session["ID"]== null)
                {
                    return RedirectToAction("Index");
                }
                if (Session["UserRole"] != null && Session["UserRole"].Equals(1))
                {
                    Squadra squadra = db.Squadre.Where(s => s.IDSquadra == id).FirstOrDefault();
                    List<UtenteInSquadra> utentiInSquadra = new List<UtenteInSquadra>();

                    //Verifica se ci sono utenti con l'ID Squadra 
                    foreach (var u in db.UtentiInSquadra)
                    {
                        if (u.IDSquadraUtente == id)
                        {
                            utentiInSquadra.Add(u);
                        }

                    }


            //VISUALIZZAZIONE NOMI
            //QUI SI FA USO DI MARS PERCHE' SENZA DI ESSO CI DA' ERRORI DI DATAREADER APERTI
            List<string> listaStringheConcat = new List<string>();
            foreach (var u in db.UtentiInSquadra.Where(n => n.IDSquadraUtente == id))
            {
                var Nome = db.Utenti.Where(ut => ut.ID == u.IDUtente).First().Nome.ToString();
                var Cognome = db.Utenti.Where(ut => ut.ID == u.IDUtente).First().Cognome.ToString();
                var isCaposquadraResult = "";
                if (u.isCaposquadra)
                {
                    isCaposquadraResult = "- Caposquadra";
                }
                listaStringheConcat.Add(Nome + " " + Cognome + " " + isCaposquadraResult);
            }


                ViewBag.listaUtenti = db.Utenti.ToList();
                ViewBag.ListaUtentiInSquadra = listaStringheConcat;


                    //La stessa cosa con le attrezzature
                    List<AttrezzaturaInSquadra> attrezzatureInSquadra = new List<AttrezzaturaInSquadra>();
                    
                    foreach(var a in db.attrezzatureInSquadra)
                    {
                        if(a.IDSquadraAttrezzatura == id)
                        {
                            attrezzatureInSquadra.Add(a);
                        }
                    }

                    List<string> nomiAttrezzature = new List<string>();
                    foreach (var  u in attrezzatureInSquadra)
                    {
                        string nome = db.Attrezzatura.Where(a => a.codiceAttrezzatura == u.codiceAttrezzatura).First().nomeAttrezzatura.ToString();
                        nomiAttrezzature.Add(nome);
                    }

                    ViewBag.ListaAttrezzature = attrezzatureInSquadra;
                    ViewBag.ListaAttrezzatureInSquadra = nomiAttrezzature;


                ViewBag.NoUserMessage = "Al momento la squadra non ha operai";
                ViewBag.NoAttrezzaturaMessage = "Al momento la squadra non ha attrezzature assegnate";

                return View(squadra);
                }
            else
                {
                    return RedirectToAction("HomepageLogged");
                }
            }
            catch (NullReferenceException ex)
            {
                return RedirectToAction("Index");
            }

            
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GestioneSquadra(Squadra squadra)
        {
            return View(squadra);
        }

        //GET
        public ActionResult AggiuntaOperaiInSquadra(int? id)
        {

            if (Session["UserRole"] != null && Session["UserRole"].Equals(1))
            {
                OperaiSquadraVM model = new OperaiSquadraVM();
                model.squadra = db.Squadre.Where(u => u.IDSquadra == id).FirstOrDefault();
                //
                model.listaUtenti = new List<UtenteInSquadraVM>();
                model.utentiInSquadra = db.UtentiInSquadra.Where(u => u.IDSquadraUtente == id).ToList();



                if (model.squadra.listaUtentiInSquadra == null)
                {
                    model.squadra.listaUtentiInSquadra = new List<UtenteInSquadra>();
                    UtenteInSquadra newUtente = new UtenteInSquadra()
                    {
                        IDSquadraUtente = (int)id,
                        IDUtente = db.Utenti.Select(u => u.ID).FirstOrDefault(),
                        isCaposquadra = false
                    };
                    model.squadra.listaUtentiInSquadra.Add(newUtente);
                }

                List<string> listaStringheConcat = new List<string>();
                foreach (var u in db.UtentiInSquadra.Where(n => n.IDSquadraUtente == id))
                {
                    var Nome = db.Utenti.Where(ut => ut.ID == u.IDUtente).First().Nome.ToString();
                    var Cognome = db.Utenti.Where(ut => ut.ID == u.IDUtente).First().Cognome.ToString();
                    listaStringheConcat.Add(Nome + " " + Cognome);
                }

                ViewBag.ListaUtentiInSquadra = listaStringheConcat;
                //else
                //{
                //    foreach(var utente in db.UtentiInSquadra.Where(us=>us.IDSquadra == id))
                //    {
                //        model.listaUtenti.Add(new UtenteInSquadraVM() { IDSquadra = id, IDutente = utente.IDUtente, IsCaposquadra = false });
                //    }
                //}


                //ViewBag.UtentiInSquadra = db.UtentiInSquadra.Where(u => u.IDSquadra == id).ToList();
                //ViewBag.ListaUtenti = new SelectList(db.Utenti,null,"Cognome","---Seleziona---");
                ViewBag.CaposquadraEsitoErrorMessage = "";
                return View(model);
            }
            else
            {
                ViewBag.CaposquadraEsitoErrorMessage = "";
                return RedirectToAction("GestioneSquadra/" + id);
            }
        }



            
        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AggiuntaOperaiInSquadra(IEnumerable<UtenteInSquadraVM> UtentiDaInserire,OperaiSquadraVM model)
        {
            var utentiDaAggiungere = new List<UtenteInSquadra>();
            int caposquadraCount = 0;

            foreach(var utente in UtentiDaInserire)
            {
                UtenteInSquadra utenteInSquadra = new UtenteInSquadra()
                {
                    IDSquadraUtente = model.squadra.IDSquadra,
                    IDUtente = utente.IDutente,
                };

                if (utente.IsCaposquadra)
                {
                    utenteInSquadra.isCaposquadra = true;
                }
                utentiDaAggiungere.Add(utenteInSquadra);
            }

            //Verifica se c'é piú di un caposquadra
            foreach(var utente in utentiDaAggiungere)
            {
                if(utente.isCaposquadra == true)
                {
                    caposquadraCount++;
                }
            }

            //esiti
            if (caposquadraCount == 1)
            {
                foreach(var utente in utentiDaAggiungere)
                {
                    db.UtentiInSquadra.Add(utente);
                    
                    
                }
                db.SaveChangesAsync();
                return RedirectToAction("GestioneSquadra/" + model.squadra.IDSquadra);

            }
            else if (caposquadraCount == 0)
            {
                foreach (var utente in utentiDaAggiungere)
                {
                    db.UtentiInSquadra.Add(utente);


                }
                db.SaveChangesAsync();
                return RedirectToAction("AggiuntaOperaiInSquadra/" + model.squadra.IDSquadra);
            }
            else
            {
                ViewBag.CaposquadraEsitoErrorMessage = "C'é piú di un caposquadra!";
                return RedirectToAction("AggiuntaOperaiInSquadra/" + model.squadra.IDSquadra);
            }
                
                db.SaveChangesAsync();
                return RedirectToAction("HomepageLogged");
        }

        public PartialViewResult AggiungiUtente()
        {

            ViewBag.ListaUtenti = new SelectList(db.Utenti, "ID", "Cognome", "--Seleziona Utente--");
            
            

            return PartialView("~/Views/Shared/_EditorRow.cshtml", new UtenteInSquadraVM());
        }

        //GET
        public ActionResult AggiuntaAttrezzatureInSquadra(int id)
        {
            if (Session["UserRole"] != null && Session["UserRole"].Equals(1))
            {
                AttrezzaturaSquadraVM model = new AttrezzaturaSquadraVM();
                model.squadra = db.Squadre.Where(u => u.IDSquadra == id).FirstOrDefault();
                //
                model.listaAttrezzatura = new List<AttrezzaturaInSquadraVM>();
                model.attrezzaturaInSquadra = db.attrezzatureInSquadra.Where(u => u.IDSquadraAttrezzatura == id).ToList();



                if (model.squadra.listaAttrezzatureInSquadra == null)
                {
                    model.squadra.listaAttrezzatureInSquadra = new List<AttrezzaturaInSquadra>();
                    AttrezzaturaInSquadra newAttrezzatura = new AttrezzaturaInSquadra()
                    {
                        IDSquadraAttrezzatura = id,
                        codiceAttrezzatura = db.Attrezzatura.Select(u => u.codiceAttrezzatura).FirstOrDefault(),
                      
                    };
                    model.squadra.listaAttrezzatureInSquadra.Add(newAttrezzatura);
                }

                List<string> listaStringheConcat = new List<string>();
                foreach (var u in db.attrezzatureInSquadra.Where(n => n.IDSquadraAttrezzatura == id))
                {
                    var Nome = db.Attrezzatura.Where(ut => ut.codiceAttrezzatura == u.codiceAttrezzatura).First().nomeAttrezzatura.ToString();
                    listaStringheConcat.Add(Nome);
                }

                ViewBag.ListaAttrezzaturaInSquadra = listaStringheConcat;
                
                
                return View(model);
            }
            else
            {
                
                return RedirectToAction("GestioneSquadra/" + id);
            }

        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AggiuntaAttrezzatureInSquadra(IEnumerable<AttrezzaturaInSquadraVM> attrezzaturaDaInserire, AttrezzaturaSquadraVM model)
        {
            var attrezzaturaDaAggiungere = new List<AttrezzaturaInSquadra>();
            

            foreach (var att in attrezzaturaDaInserire)
            {
                AttrezzaturaInSquadra attrezzatura = new AttrezzaturaInSquadra()
                {
                    IDSquadraAttrezzatura = model.squadra.IDSquadra,
                    codiceAttrezzatura = att.codiceAttrezzatura,
                };

                
                attrezzaturaDaAggiungere.Add(attrezzatura);
            }

            foreach (var att in attrezzaturaDaAggiungere)
            {
                att.dataAssegnazione = DateTime.Now;
                db.attrezzatureInSquadra.Add(att);


            }

            db.SaveChangesAsync();
            return RedirectToAction("GestioneSquadra/" + model.squadra.IDSquadra);
        }

        public PartialViewResult AggiungiAttrezzatura()
        {

            ViewBag.ListaAttrezzature = new SelectList(db.Attrezzatura, "codiceAttrezzatura", "nomeAttrezzatura", "--Seleziona Attrezzatura--");



            return PartialView("~/Views/Shared/_EditorRowAttrezzatura.cshtml", new AttrezzaturaInSquadraVM());
        }


        public ActionResult ElencoUtenti()
        {
            try
            {
                if (Session["UserRole"] != null && Session["UserRole"].Equals(1))
                {
                    GestoreVM model = new GestoreVM
                    {
                        Ruoli = db.Ruoli.ToList(),
                        Utenti = db.Utenti.ToList()

                    };
                    return View(model);
                }
                else
                {
                    return RedirectToAction("HomepageLogged");
                }
            }
            catch (NullReferenceException ex)
            {
                return RedirectToAction("Index");
            }
            
        }

        public ActionResult ElencoSquadre()
        {
            try
            {
                if (Session["UserRole"] != null && Session["UserRole"].Equals(1))
                {
                    return View(db.Squadre.ToList());
                }
                else
                {
                    return RedirectToAction("HomepageLogged");
                }
            }
            catch (NullReferenceException ex)
            {
                return RedirectToAction("Index");
            }

            
        }

        public ActionResult ElencoAttrezzature()
        {
            try
            {
                if (Session["UserRole"]!=null && Session["UserRole"].Equals(1))
                {
                    AttrezzatureVM model = new AttrezzatureVM()
                    {
                        Attrezzature = db.Attrezzatura.ToList(),
                        Fornitori = db.Fornitori.ToList(),
                        AttrezzaturaInSquadras = db.attrezzatureInSquadra.ToList(),
                        Squadre = db.Squadre.ToList()
                        
                        
                    };
                    return View(model);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (NullReferenceException ex)
            {
                return RedirectToAction("Index");
            }
            

        }

        //GET
        public ActionResult Edit(int id)
        {
            Utente utente = db.Utenti.Where(u => u.ID == id).FirstOrDefault();
            try
            {
                if (Session["UserRole"] != null && Session["UserRole"].Equals(1))
                {
                    ViewBag.RoleList = new SelectList(db.Ruoli, "IDRuolo", "descrizione", utente.IDRuolo);
                    return View(utente);

                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (NullReferenceException ex)
            {
                return RedirectToAction("Index");
            }           
        }


        [HttpPost]
        public ActionResult Edit(Utente utente)
        {
            try
            {
                if (Session["UserRole"] != null && Session["UserRole"].Equals(1))
                {
                    Utente utenteDaAggiornare = db.Utenti.Where(u => u.ID == utente.ID).FirstOrDefault();

                    utenteDaAggiornare.Nome = utente.Nome;
                    utenteDaAggiornare.Cognome = utente.Cognome;
                    utenteDaAggiornare.IDRuolo = utente.IDRuolo;
                    utenteDaAggiornare.Email = utente.Email;

                    db.SaveChangesAsync();
                    return RedirectToAction("ElencoUtenti");
                    
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (NullReferenceException ex)
            {
                return RedirectToAction("Index");
            }
        }

        //GET
        public ActionResult EditAttrezzature(int id)
        {
            EditAttrezzaturaVM attrezzatura = new EditAttrezzaturaVM()
            {
                AttrezzaturaDaEditare = db.Attrezzatura.Where(a => a.codiceAttrezzatura == id).FirstOrDefault(),
                AttrezzaturaInSquadra = db.attrezzatureInSquadra.Where(a=>a.codiceAttrezzatura == id).FirstOrDefault()
            };

            try
            {
                if (Session["UserRole"]!=null && Session["UserRole"].Equals(1))
                {
                    ViewBag.FornitoreList = new SelectList(db.Fornitori, "IDFornitore", "nomeFornitore", attrezzatura.AttrezzaturaDaEditare.IDFornitore);
                    ViewBag.SquadreList = new SelectList(db.Squadre, "IDSquadra", "nomeSquadra", attrezzatura.IDSquadra);
                    var IDSquadra = db.attrezzatureInSquadra.Any(i=>i.codiceAttrezzatura == id);
                    if (IDSquadra)
                    {
                        attrezzatura.IDSquadra = db.attrezzatureInSquadra.Where(ais => ais.codiceAttrezzatura == id).FirstOrDefault().IDSquadraAttrezzatura;
                    }
                    return View(attrezzatura);
                    
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (NullReferenceException ex)
            {
                return RedirectToAction("Index");
            }
           
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAttrezzature(EditAttrezzaturaVM attrezzatura)
        {
            Attrezzatura attrezzaturaDaAggiornare = db.Attrezzatura.Where(a => a.codiceAttrezzatura == attrezzatura.AttrezzaturaDaEditare.codiceAttrezzatura).FirstOrDefault();
            

            attrezzaturaDaAggiornare.statoDiFunzionamento = attrezzatura.AttrezzaturaDaEditare.statoDiFunzionamento;
            attrezzaturaDaAggiornare.categoria = attrezzatura.AttrezzaturaDaEditare.categoria;
            attrezzaturaDaAggiornare.dataDiScadenza = attrezzatura.AttrezzaturaDaEditare.dataDiScadenza;
            
            attrezzaturaDaAggiornare.tipoManuale = attrezzatura.AttrezzaturaDaEditare.tipoManuale;
            attrezzaturaDaAggiornare.IDFornitore = attrezzatura.AttrezzaturaDaEditare.IDFornitore;

            if (attrezzatura.IDSquadra != null)
            {
                attrezzaturaDaAggiornare.assegnata = true;
                AttrezzaturaInSquadra attrezzaturaInSquadra = db.attrezzatureInSquadra.Where(a => a.codiceAttrezzatura == attrezzatura.AttrezzaturaDaEditare.codiceAttrezzatura).FirstOrDefault();
                if (attrezzaturaInSquadra != null)
                {
                    attrezzaturaInSquadra.IDSquadraAttrezzatura = (int)attrezzatura.IDSquadra;
                }
                else
                {
                    AttrezzaturaInSquadra AISdaAggiungere = new AttrezzaturaInSquadra
                    {
                        IDSquadraAttrezzatura = (int)attrezzatura.IDSquadra,
                        codiceAttrezzatura = attrezzatura.AttrezzaturaDaEditare.codiceAttrezzatura
                    };
                    db.attrezzatureInSquadra.Add(AISdaAggiungere);
                }
            }
            else { attrezzaturaDaAggiornare.assegnata = false; }
            

            db.SaveChangesAsync();
            return RedirectToAction("ElencoAttrezzature");
        }
       
       
       
        public ActionResult CensimentoFornitori()
        {
            CensimentoFornitoriVM model = new CensimentoFornitoriVM()
            {
                fornitore = new Fornitore(),
            };
            try
            {
                if (Session["UserRole"] != null && Session["UserRole"].Equals(1))
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (NullReferenceException ex)
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult CensimentoFornitori(CensimentoFornitoriVM model)
        {

            if (ModelState.IsValid)
            {
                db.Fornitori.Add(model.fornitore);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError("", "Qualcosa é andato storto");
            }
            return View(model);
        }

        //GET
        public ActionResult RichiediAudit()
        {
            if (Session["UserRole"] != null && Session["UserRole"].Equals(3)){
                ViewBag.SquadreList = new SelectList(db.Squadre, "IDSquadra", "nomeSquadra", "---Scegli Squadra---");
                ViewBag.attrezzatureList = new SelectList(string.Empty,"","--seleziona prima la squadra--");
                return View();
            }
            else { return RedirectToAction("Index"); }
            
        }

        //JSONRESULT (non so cosa faccia)
        [HttpGet]
        public JsonResult GetAttrezzature(int ID)
        {
           var data = db.attrezzatureInSquadra.Where(a=>a.IDSquadraAttrezzatura == ID)
                .Select(at=>new { Value = at.codiceAttrezzatura
                ,Text = db.Attrezzatura.Where(p=>p.codiceAttrezzatura == at.codiceAttrezzatura)
                .FirstOrDefault().nomeAttrezzatura.ToString() });

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RichiediAudit(AuditVM model)
        {
            
                Audit auditDaAggiungere = new Audit()
                {
                    esitoAudit = Esito.Attesa,
                    dataApertura = model.audit.dataApertura,
                    dataChiusura = model.audit.dataChiusura,
                    IDSquadra = (int)model.selectedSquadra,
                    checkVisivoStrumentale = model.audit.checkVisivoStrumentale

                };
                db.Audits.Add(auditDaAggiungere);
                await db.SaveChangesAsync();

                //AuditGestore: Prende l'ID dell'Audit e il quello del Gestore richiedente
                AuditGestore auditGestore = new AuditGestore();
                auditGestore.IDAudit = auditDaAggiungere.IDAudit;
                //Prova a prendere l'ID dell'utente richiedente
                //lo faccio prima per evitare di creare istanze a vuoto
                if (Int32.TryParse(Session["ID"].ToString(), out int idGestore))
                {
                    auditGestore.IDUtente = idGestore;
                    db.AuditsGestore.Add(auditGestore);
                    await db.SaveChangesAsync();
                }
                else
                {
                    //ritorno alla home se non riesce a convertire il Session["ID"]
                    ViewBag.CouldntRetrieveIDMessage = "Non é stato possibile recuperare l'ID, riprova piú tardi";
                    return RedirectToAction("Index");
                }


                //AuditSquadra: Audit della squadra con le informazioni della attrezzatura
                foreach (var att in db.attrezzatureInSquadra.Where(s=>s.IDSquadraAttrezzatura == model.selectedSquadra)) 
                {
                    AuditSquadra auditSquadra = new AuditSquadra();
                    auditSquadra.IDAssegnazione = att.IDAssegnazione;
                    auditSquadra.IDAudit = auditDaAggiungere.IDAudit;
                    auditSquadra.esitoAuditControllo = auditDaAggiungere.esitoAudit;
                    

                    db.AuditsSquadra.Add(auditSquadra);
                    
                //auditSquadra.IDAssegnazione = 
                }
                await db.SaveChangesAsync();

                ViewBag.EsitoMessage = "Audit richiesto con successo";
                return RedirectToAction("Index");
            
            
        }

        //GET
        public async Task<ActionResult> StatoEsitiAudit(string nameSearchString, EsitoAuditVM enumResult, DateTime? DateFilter)
        {
            if (Session["ID"] != null && Session["UserRole"].Equals(3))
            {
                 
                EsitoAuditVM model = new EsitoAuditVM();
                //parametri default
                model.attrezzature = await db.Attrezzatura.ToListAsync();
                model.attrezzaturaInSquadras = await db.attrezzatureInSquadra.ToListAsync();
                model.audits = await db.Audits.ToListAsync();
                model.auditGestore = await db.AuditsGestore.ToListAsync();
                model.auditSquadre = await db.AuditsSquadra.ToListAsync();
                model.squadras = await db.Squadre.ToListAsync();

                var audits = from a in db.Audits select a;
                var auditSquadra = from a in db.AuditsSquadra select a;
                var auditGestore = from a in db.AuditsGestore select a;

                var att = from at in db.Attrezzatura select at;
                var attSquadra = from at in db.attrezzatureInSquadra select at;

                var squadra = from s in db.Squadre select s;

                //var attrezzature = from m in 
                if (!String.IsNullOrEmpty(nameSearchString)) //Stringa ricerca non vuota o null
                {
                    att = db.Attrezzatura.Where(at => at.nomeAttrezzatura.Contains(nameSearchString));
                    attSquadra = db.attrezzatureInSquadra.Where(ats => ats.codiceAttrezzatura == att.FirstOrDefault().codiceAttrezzatura);

         
                    auditSquadra = db.AuditsSquadra.Where(aS => aS.IDAssegnazione == attSquadra.FirstOrDefault().IDAssegnazione);
                    auditGestore = db.AuditsGestore.Where(ag => ag.IDAudit == auditSquadra.FirstOrDefault().IDAudit);
                    audits = db.Audits.Where(a => a.IDAudit == auditGestore.FirstOrDefault().IDAudit);
                    //audits  = model.attrezzature.Where(s => s.nomeAttrezzatura.Contains(searchString));

                    
                }
                
                if (!String.IsNullOrEmpty(enumResult.esiti.ToString()))
                {
                    audits = db.Audits.Where(a => a.esitoAudit == enumResult.esiti);
                    auditGestore = db.AuditsGestore.Where(ag => ag.IDAudit == audits.FirstOrDefault().IDAudit);
                    auditSquadra = db.AuditsSquadra.Where(aS => aS.IDAudit == audits.FirstOrDefault().IDAudit);

                    attSquadra = db.attrezzatureInSquadra.Where(ats => ats.IDAssegnazione == auditSquadra.FirstOrDefault().IDAssegnazione);
                    att = db.Attrezzatura.Where(at => at.codiceAttrezzatura == attSquadra.FirstOrDefault().codiceAttrezzatura);
                }
                
                if (!String.IsNullOrEmpty(DateFilter.ToString()))
                {
                    audits = db.Audits.Where(a => a.dataChiusura == DateFilter);
                    auditGestore = db.AuditsGestore.Where(ag => ag.IDAudit == audits.FirstOrDefault().IDAudit);
                    auditSquadra = db.AuditsSquadra.Where(aS => aS.IDAudit == audits.FirstOrDefault().IDAudit);

                    attSquadra = db.attrezzatureInSquadra.Where(ats => ats.IDAssegnazione == auditSquadra.FirstOrDefault().IDAssegnazione);
                    att = db.Attrezzatura.Where(at => at.codiceAttrezzatura == attSquadra.FirstOrDefault().codiceAttrezzatura);

                }
                






                model.attrezzature = await att.ToListAsync();
                model.attrezzaturaInSquadras = await attSquadra.ToListAsync();
                model.audits = await audits.ToListAsync();
                model.auditGestore = await auditGestore.ToListAsync();
                model.auditSquadre = await auditSquadra.ToListAsync();
                model.squadras = await squadra.ToListAsync();

                return View(model);
            }
            else return RedirectToAction("HomepageLogged");

           
        }


    }

}

