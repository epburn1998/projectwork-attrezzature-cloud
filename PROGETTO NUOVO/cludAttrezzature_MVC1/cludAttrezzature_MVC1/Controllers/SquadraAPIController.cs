﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AttrezzatureFramework;
using AttrezzatureFramework.DAL;

namespace cludAttrezzature_MVC1.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SquadraAPIController : ApiController
    {
        private AttrezzatureDbContext db = new AttrezzatureDbContext();

        // GET: api/SquadraAPI
        public IQueryable<SquadraDTO> GetSquadras()
        {
            var squadre = from b in db.Squadre
                          select new SquadraDTO()
                          {
                              IDSquadra =b.IDSquadra,
                              nomeSquadra=b.nomeSquadra
                          };
            return squadre;
        }

        // GET: api/SquadraAPI/5
        [ResponseType(typeof(Squadra))]
        public async Task<IHttpActionResult> GetSquadra(int id)
        {
            var squadra = await db.Squadre.Select(b => new SquadraDTO()
            {
                IDSquadra = b.IDSquadra,
                nomeSquadra = b.nomeSquadra
            }).SingleOrDefaultAsync(b => b.IDSquadra == id);
            if (squadra == null)
            {
                return NotFound();
            }

            return Ok(squadra);
        }

        // PUT: api/SquadraAPI/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSquadra(int id, Squadra squadra)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != squadra.IDSquadra)
            {
                return BadRequest();
            }

            db.Entry(squadra).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SquadraExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SquadraAPI
        [ResponseType(typeof(Squadra))]
        public async Task<IHttpActionResult> PostSquadra(Squadra squadra)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Squadre.Add(squadra);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = squadra.IDSquadra }, squadra);
        }

        // DELETE: api/SquadraAPI/5
        [ResponseType(typeof(Squadra))]
        public async Task<IHttpActionResult> DeleteSquadra(int id)
        {
            Squadra squadra =  db.Squadre.Find(id);
            if (squadra == null)
            {
                return NotFound();
            }

            db.Squadre.Remove(squadra);
            await db.SaveChangesAsync();

            return Ok(squadra);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SquadraExists(int id)
        {
            return db.Squadre.Count(e => e.IDSquadra == id) > 0;
        }
    }
}