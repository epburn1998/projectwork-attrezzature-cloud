﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AttrezzatureFramework;
using AttrezzatureFramework.DAL;

namespace cludAttrezzature_MVC1.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AttrezzaturaInSquadraAPIController : ApiController
    {
        private AttrezzatureDbContext db = new AttrezzatureDbContext();

        // GET: api/AttrezzaturaInSquadraAPI
        public IQueryable<AttrezzaturaInSquadraDTO> GetAttrezzaturaInSquadras()
        {
            var attSquad = from b in db.attrezzatureInSquadra
                           select new AttrezzaturaInSquadraDTO()
                           {
                               codiceAttrezzatura = b.codiceAttrezzatura,
                               IDAssegnazione = b.IDAssegnazione,
                               IDSquadra = b.IDSquadraAttrezzatura
                           };
            return attSquad;
        }

        // GET: api/AttrezzaturaInSquadraAPI/5
        [ResponseType(typeof(AttrezzaturaInSquadraDTO))]
        public async Task<IHttpActionResult> GetAttrezzaturaInSquadra(int id)
        {
            var attSquad = await db.attrezzatureInSquadra.Select(b => new AttrezzaturaInSquadraDTO()
            {
                codiceAttrezzatura = b.codiceAttrezzatura,
                IDAssegnazione = b.IDAssegnazione,
                IDSquadra = b.IDSquadraAttrezzatura
            }).SingleOrDefaultAsync(b=>b.IDAssegnazione == id);
            if (attSquad == null)
            {
                return NotFound();
            }

            return Ok(attSquad);
        }

        // PUT: api/AttrezzaturaInSquadraAPI/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAttrezzaturaInSquadra(int id, attSquadCambio attSquad)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != attSquad.idAssegnazione)
            {
                return BadRequest();
            }

            //db.Entry(attrezzaturaInSquadra).State = EntityState.Modified;
            db.attrezzatureInSquadra.Where  //continua
                (a => a.codiceAttrezzatura == attSquad.codiceAttrezzatura)
                .FirstOrDefault().IDSquadraAttrezzatura = attSquad.IDSquadra;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AttrezzaturaInSquadraExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AttrezzaturaInSquadraAPI
        [ResponseType(typeof(AttrezzaturaInSquadra))]
        public async Task<IHttpActionResult> PostAttrezzaturaInSquadra(AttrezzaturaInSquadra attrezzaturaInSquadra)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.attrezzatureInSquadra.Add(attrezzaturaInSquadra);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = attrezzaturaInSquadra.IDAssegnazione }, attrezzaturaInSquadra);
        }

        // DELETE: api/AttrezzaturaInSquadraAPI/5
        [ResponseType(typeof(AttrezzaturaInSquadra))]
        public async Task<IHttpActionResult> DeleteAttrezzaturaInSquadra(int id)
        {
            AttrezzaturaInSquadra attrezzaturaInSquadra = db.attrezzatureInSquadra.Find(id);
            if (attrezzaturaInSquadra == null)
            {
                return NotFound();
            }

            db.attrezzatureInSquadra.Remove(attrezzaturaInSquadra);
            await db.SaveChangesAsync();

            return Ok(attrezzaturaInSquadra);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AttrezzaturaInSquadraExists(int id)
        {
            return db.attrezzatureInSquadra.Count(e => e.IDAssegnazione == id) > 0;
        }
    }
}