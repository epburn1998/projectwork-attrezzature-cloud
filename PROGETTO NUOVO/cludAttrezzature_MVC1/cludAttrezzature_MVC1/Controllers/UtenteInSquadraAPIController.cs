﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AttrezzatureFramework;
using AttrezzatureFramework.DAL;

namespace cludAttrezzature_MVC1.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UtenteInSquadraAPIController : ApiController
    {
        private AttrezzatureDbContext db = new AttrezzatureDbContext();

        // GET: api/UtenteInSquadraAPI
        public IQueryable<UtenteInSquadraDTO> GetUtenteInSquadras()
        {
            var utentiInSquadra = from b in db.UtentiInSquadra
                                  select new UtenteInSquadraDTO()
                                  {
                                      IDUtente = b.IDUtente,
                                      IDSquadra = b.IDSquadraUtente,
                                      isCaposquadra = b.isCaposquadra
                                  };
            return utentiInSquadra;

        }

        // GET: api/UtenteInSquadraAPI/5
        [ResponseType(typeof(UtenteInSquadraDTO))]
        public async Task<IHttpActionResult> GetUtenteInSquadra(int id)
        {
            UtenteInSquadraDTO utenteInSquadra = await db.UtentiInSquadra.Select(b => new UtenteInSquadraDTO()
            {
                IDUtente = b.IDUtente,
                IDSquadra = b.IDSquadraUtente,
                isCaposquadra = b.isCaposquadra
            }).SingleOrDefaultAsync(b => b.IDUtente == id);
            if (utenteInSquadra == null)
            {
                return NotFound();
            }

            return Ok(utenteInSquadra);
        }

        // PUT: api/UtenteInSquadraAPI/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUtenteInSquadra(int id, UtenteInSquadra utenteInSquadra)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != utenteInSquadra.IDSquadraUtente)
            {
                return BadRequest();
            }

            db.Entry(utenteInSquadra).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UtenteInSquadraExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UtenteInSquadraAPI
        [ResponseType(typeof(UtenteInSquadra))]
        public async Task<IHttpActionResult> PostUtenteInSquadra(UtenteInSquadra utenteInSquadra)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UtentiInSquadra.Add(utenteInSquadra);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UtenteInSquadraExists(utenteInSquadra.IDSquadraUtente))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = utenteInSquadra.IDSquadraUtente }, utenteInSquadra);
        }

        // DELETE: api/UtenteInSquadraAPI/5
        [ResponseType(typeof(UtenteInSquadra))]
        public async Task<IHttpActionResult> DeleteUtenteInSquadra(int id)
        {
            UtenteInSquadra utenteInSquadra =  db.UtentiInSquadra.Find(id);
            if (utenteInSquadra == null)
            {
                return NotFound();
            }

            db.UtentiInSquadra.Remove(utenteInSquadra);
            await db.SaveChangesAsync();

            return Ok(utenteInSquadra);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UtenteInSquadraExists(int id)
        {
            return db.UtentiInSquadra.Count(e => e.IDSquadraUtente == id) > 0;
        }
    }
}