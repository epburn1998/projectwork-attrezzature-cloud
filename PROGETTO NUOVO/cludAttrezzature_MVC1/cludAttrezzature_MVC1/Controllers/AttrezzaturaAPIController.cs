﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using AttrezzatureFramework;
using AttrezzatureFramework.DAL;

namespace cludAttrezzature_MVC1.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AttrezzaturaAPIController : ApiController
    {
        private AttrezzatureDbContext db = new AttrezzatureDbContext();

        // GET: api/AttrezzaturaAPI
        public IQueryable<AttrezzaturaDTO> GetAttrezzaturas()
        {
            var attrezzature = from b in db.Attrezzatura
                         select new AttrezzaturaDTO()
                         {
                             codiceAttrezzatura = b.codiceAttrezzatura,
                             nomeAttrezzatura = b.nomeAttrezzatura
                             
                            
                         };
            return attrezzature;
            
        }

        // GET: api/AttrezzaturaAPI/5
        [ResponseType(typeof(AttrezzaturaDTO))]
        public async Task<IHttpActionResult> GetAttrezzatura(int id)
        {
            var attrezzatura = await db.Attrezzatura.Select(b => new AttrezzaturaDTO()
            {
                codiceAttrezzatura= b.codiceAttrezzatura,
                nomeAttrezzatura= b.nomeAttrezzatura
            }).SingleOrDefaultAsync(b => b.codiceAttrezzatura == id);
            if (attrezzatura == null)
            {
                return NotFound();
            }

            return Ok(attrezzatura);
        }

        // PUT: api/AttrezzaturaAPI/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAttrezzatura(int id, Attrezzatura attrezzatura)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != attrezzatura.codiceAttrezzatura)
            {
                return BadRequest();
            }

            db.Entry(attrezzatura).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AttrezzaturaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AttrezzaturaAPI
        [ResponseType(typeof(Attrezzatura))]
        public async Task<IHttpActionResult> PostAttrezzatura(Attrezzatura attrezzatura)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Attrezzatura.Add(attrezzatura);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = attrezzatura.codiceAttrezzatura }, attrezzatura);
        }

        // DELETE: api/AttrezzaturaAPI/5
        [ResponseType(typeof(Attrezzatura))]
        public async Task<IHttpActionResult> DeleteAttrezzatura(int id)
        {
            Attrezzatura attrezzatura = db.Attrezzatura.Find(id);
            if (attrezzatura == null)
            {
                return NotFound();
            }

            db.Attrezzatura.Remove(attrezzatura);
            await db.SaveChangesAsync();

            return Ok(attrezzatura);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AttrezzaturaExists(int id)
        {
            return db.Attrezzatura.Count(e => e.codiceAttrezzatura == id) > 0;
        }
    }
}